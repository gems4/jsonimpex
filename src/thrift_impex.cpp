#include <iostream>
#include "thrift_import.h"
#include "TJSONSerializer.h"
#include "jsonio/io_settings.h"

namespace jsonio {

void writeDataToJsonSchema( const ThriftSchema* schema, JsonDom* object,
           const std::string& structName, ::apache::thrift::TBase* data );
void readDataFromJsonSchema( const ThriftSchema* schema, const JsonDom* object,
    const std::string& structName,  ::apache::thrift::TBase* data );
StructDataIEFile* connectFormatStructDataFile( int amode, const ThriftSchema* schema, const JsonDom* object,
                                               const std::string& lua_lib_path );
KeyValueIEFile* connectFormatKeyValueFile( int amode, const ThriftSchema* schema, const JsonDom* object,
                                           const std::string& lua_lib_path );
TableIEFile* connectFormatTableFile( int amode, const ThriftSchema* schema, const JsonDom* object,
                                     const std::string& lua_lib_path );
AbstractIEFile* connectFormatDataFile( int amode, const std::string& impexSchemaName, const JsonDom* object );


void writeDataToJsonSchema( const ThriftSchema* schema, JsonDom* object,
           const std::string& structName, ::apache::thrift::TBase* data )
{
    //Set the Serializator
    std::shared_ptr<TMemoryBuffer> buffer(new TMemoryBuffer());
    ::std::shared_ptr<TJSONSerializer> proto = std::make_shared<TJSONSerializer>( buffer, schema );
    proto->setDom( object, structName );
    // Get bson object from structure
    /*int  i = */data->write(proto.get());
    //std::cout << "Wrote " << i << " bytes" << std::endl;
}

void readDataFromJsonSchema( const ThriftSchema* schema, const JsonDom* object,
    const std::string& structName,  ::apache::thrift::TBase* data )
{
    //Set the Serializator
    std::shared_ptr<TMemoryBuffer> buffer(new TMemoryBuffer());
    ::std::shared_ptr<TJSONSerializer> proto = std::make_shared<TJSONSerializer>( buffer, schema );
    proto->setDom( const_cast<JsonDom*>(object), structName );
   // Get bson object from structure
   /*int  i = */data->read(proto.get());
   //std::cout << "Read " << i << " bytes" << std::endl;
}

StructDataIEFile* connectFormatStructDataFile( int amode, const ThriftSchema* schema, const JsonDom* object,
                                               const std::string& lua_lib_path )
{
    FormatStructDataFile fformatdata;
    readDataFromJsonSchema( schema, object, "FormatStructDataFile",  &fformatdata );
    //Create input structure
    StructDataIEFile*  inputIEFile=  new  StructDataIEFile( amode, schema, lua_lib_path, fformatdata );
    return inputIEFile;
}

KeyValueIEFile* connectFormatKeyValueFile( int amode, const ThriftSchema* schema, const JsonDom* object,
                                           const std::string& lua_lib_path )
{
    FormatKeyValueFile fformatdata;
    readDataFromJsonSchema( schema, object, "FormatKeyValueFile",  &fformatdata );
    //Create input structure
    KeyValueIEFile*  inputIEFile=  new  KeyValueIEFile( amode, schema, lua_lib_path, fformatdata );
    return inputIEFile;
}


TableIEFile* connectFormatTableFile( int amode, const ThriftSchema* schema, const JsonDom* object,
                                     const std::string& lua_lib_path )
{
    FormatTableFile fformatdata;
    readDataFromJsonSchema( schema, object, "FormatTableFile",  &fformatdata );
    //Create input structure
    TableIEFile*  inputIEFile=  new  TableIEFile( amode, schema, lua_lib_path, fformatdata );
    return inputIEFile;
}

AbstractIEFile* connectFormatDataFile( int amode, const std::string& impexSchemaName, const JsonDom* object )
{
    std::string lua_lib_path =  ioSettings().resourcesDir()+"/lua/";
    //ioSettings().directoryPath("common.LuaScriptsDirectory", std::string("/lua"))+"/";
    if( impexSchemaName == "FormatStructDataFile" )
         return connectFormatStructDataFile( amode, ioSettings().Schema(), object,  lua_lib_path );
    if( impexSchemaName == "FormatKeyValueFile" )
         return connectFormatKeyValueFile( amode, ioSettings().Schema(), object,  lua_lib_path );
    if( impexSchemaName == "FormatTableFile" )
         return connectFormatTableFile( amode, ioSettings().Schema(), object,  lua_lib_path );
    // other do not realize
    return nullptr;
 }



ImpexFormatFile::ImpexFormatFile( int amode, const std::string& impexSchemaName, const JsonDom* object ):
    pdata( connectFormatDataFile( amode, impexSchemaName, object ))
{

}

const std::string&  ImpexFormatFile::getDataName() const
{
  return pdata->getDataName();
}


void ImpexFormatFile::openFile( const std::string& inputFname )
{
  pdata->openFile( inputFname );
}

void ImpexFormatFile::closeFile()
{
    pdata->closeFile();
}

void ImpexFormatFile::loadString(const std::string &inputData)
{
  pdata->loadString(inputData);
}

std::string ImpexFormatFile::getString()
{
  return pdata->getString();
}

bool ImpexFormatFile::readBlock( std::string& blockdata )
{
  return pdata->readBlock( blockdata );
}

bool ImpexFormatFile::readBlock(  const std::string& inputjson, std::string& schemajson, std::string& key, int ndx )
{
  return pdata->readBlock( inputjson,  schemajson,  key,  ndx );
}

bool ImpexFormatFile::nextFromFile( std::string& inputjson )
{
  return pdata->nextFromFile( inputjson );
}


bool ImpexFormatFile::writeBlockToFile( const ValuesMap& writeData )
{
   return pdata->writeBlockToFile( writeData );
}

void ImpexFormatFile::extractDataToWrite( const std::string& blockdata, ValuesMap& writeData )
{
   pdata->extractDataToWrite(  blockdata,  writeData );
}

bool ImpexFormatFile::writeBlock( const std::string& blockdata )
{
  return pdata->writeBlock(  blockdata );
}

} // namespace jsonio
