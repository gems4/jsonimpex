//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file thrift_import.cpp
/// Implementation of classes AbstractIEFile, StructDataIEFile, KeyValueIEFile -
/// import of foreign format files
/// Prepared an impex.thrift file describing 4 file formats for import/export
/// of data to internal DOM based on our JSON schemas.
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include "jsonio/jsondomfree.h"
#include "jsonimpex/yamlxml2file.h"
#include "thrift_import.h"
#include "thrift_import.h"
//#include <boost/regex.hpp>
//namespace rgx = boost;
#include <regex>
namespace rgx = std;

namespace jsonio {

bool regexp_test(const std::string& str, std::string rgx_str);

bool regexp_test(const std::string& str, std::string rgx_str)
{
    std::regex rx(rgx_str);
    return std::regex_match(str.c_str(),rx);
}

void AbstractIEFile::setReadedData()
{
   JsonDomSchema* fld;
  // set up predefined data
   auto itr = block.defaults.begin();
   while ( itr != block.defaults.end() )
   {
       fld = structNode->field(itr->first);
       if( fld == nullptr)
       {
           std::cout << "Not found " <<  itr->first << std::endl;
           itr++;
           continue;
       }
       fld->setValue(itr->second);
       itr++;
   }

  // set up readed data
  std::string impkey;
  std::vector<std::string> impvalue;
  auto it = externData.begin();
  while ( it != externData.end() )
  {
     impkey = it->first;
     impvalue = it->second;
     it++;

     auto idom = block.matches.find( impkey );
     if( idom == block.matches.end() ) // not converted
       continue;
     std::string domkey = idom->second.field; //idom->second.fieldkey;
     if( idom->second.ignore || domkey.empty() )  // skip ignored
         continue;

     fld = structNode->field(domkey);
     if( !fld )
     {
       auto pos = domkey.find_last_of(".");
       if(pos != std::string::npos)
       {
         std::string key = std::string(domkey,  pos+1 );
         domkey = std::string(domkey, 0, pos);
         fld = structNode->field(domkey);
         if( fld && fld->isMap() )
         {
           run_convert( idom->second.convert, impvalue );
           run_lua_script_string( idom->second.script, impvalue, false );
           fld->addMapKeyValue( key, impvalue[0]  );
         }
        }
        continue;
      }

      run_convert( idom->second.convert, impvalue );
      if( fld->elemTypeId() == ::apache::thrift::protocol::T_STRING )
       run_lua_script_string( idom->second.script, impvalue, fld->isArray() );
      else
        run_lua_script( idom->second.script, impvalue, fld->isArray() );

      if( fld->isArray() && !fld->isMap() )
      {
        fld->resetArray( impvalue );
      }
      else
       if(impvalue.size()>0)
          fld->setValue(impvalue[0]);
//     it++;
  }
}

/// change value, using convert
void AbstractIEFile::run_convert( const std::map<std::string,std::string>&  convert,
                                  std::vector<std::string>& values )
{
    if( convert.empty() )
      return;
    for(uint ii=0; ii< values.size(); ii++ )
    {
        auto itmap = convert.find( values[ii] );
        if( itmap != convert.end() )
         values[ii] = itmap->second;
    }
}

// change all values use script
void AbstractIEFile::run_lua_script( const std::string& luascript,
            std::vector<std::string>& values, bool isArray )
{
  if(luascript.empty()  || values.size() < 1 )
     return;

  double dv;
  std::vector<double> dvals;

  // convert to numbers
  auto it = values.begin();
  while( it != values.end() )
  {
    TArray2Base::string2value( *it++, dv );
    dvals.push_back(dv);
  }

  // run script
  bool ret;
  if( isArray )
      ret = _lua.runFunc(luascript, dvals);
  else
      ret = _lua.runFunc(luascript, dvals[0]);

  if( ret)   // script run ok
  {
      // convert to strings
      values.clear();
      auto itd = dvals.begin();
      while( itd != dvals.end() )
        values.push_back(TArray2Base::value2string( *itd++ ));
  }
}

// change all values use script
void AbstractIEFile::run_lua_script_string( const std::string& luascript,
            std::vector<std::string>& values, bool isArray )
{
  if(luascript.empty()  || values.size() < 1 )
     return;

  // run script
  if( isArray )
     _lua.runFunc(luascript, values);
  else
     _lua.runFunc(luascript, values[0]);
}

// Run script for operation on data values in block
void AbstractIEFile::run_block_script( const std::string& luascript, JsonDomSchema* obj )
{
    if(luascript.empty() )
       return;

    std::string recBsonText;
    printNodeToJson( recBsonText, obj );

    // run script
    std::string newjson = _lua.run( luascript, recBsonText, false );
    if( !newjson.empty() )
    {
      obj->clearField();
      parseJsonToNode( newjson, obj );
    }
}

std::string AbstractIEFile::impexFieldOrganization( const std::string& impexFieldKey )
{
    auto itPair = block.pairs.find(impexFieldKey);
    if( itPair != block.pairs.end() )
         return itPair->second.organization;
    return "";
 }

std::string AbstractIEFile::impexFieldType( const std::string& impexFieldKey )
{
    auto itPair = block.pairs.find(impexFieldKey);
    if( itPair != block.pairs.end() )
         return itPair->second.datatype;
    return "";
 }

// export part-----------------------------------------

void AbstractIEFile::assembleGroups( ValuesMap& writeData  )
{
    std::string groupKey, itKey;
    std::vector<std::string> groupValues;
    size_t groupKeySize;
    size_t ndx;

    auto itPair = block.pairs.begin();
    while( itPair != block.pairs.end() )
    {
       if( itPair->second.organization == "group" )
       {
          groupKey = itPair->first;
          groupValues = extractValue( groupKey, writeData ); // get old
          groupKeySize = groupKey.size();

          auto itv = writeData.begin();
          while( itv != writeData.end() )
          {
            itKey = itv->first;
            if( itKey.substr( 0, groupKeySize ) == groupKey )
            {
              if( is<size_t>( ndx, itKey.substr( groupKeySize ) ) )
              {
                if( ndx+1 > groupValues.size() )
                  groupValues.resize(ndx+1, " ");
                if( itv->second.size() > 0 )
                 groupValues[ndx] =  itv->second[0];
                itv = writeData.erase(itv);
                continue;
              }
            }
            itv++;
           }
           writeData[groupKey]  = groupValues;
       }
       itPair++;
    }
}

void AbstractIEFile::loadString(const std::string &)
{
    jsonioErr( "loadString", "Loading string to import from  or export to stringstream  not implemented yet.");
}

std::string AbstractIEFile::getString()
{
  jsonioErr( "getString", "Loading string to import from  or export to stringstream  not implemented yet.");
}


// Insert key value paies to extern list
void AbstractIEFile::addMapKeyValue( const std::string& _key, const std::vector<std::string>& vec )
{
    auto itr = externData.find(_key);
    if( itr != externData.end() )
    {
        // add only for type "group"
        if( impexFieldOrganization( _key ) == "group")
        {
          itr->second.insert( itr->second.end(), vec.begin(), vec.end() );
          return;
        }
    }
    // can overload previous
    externData[_key] = vec;
}


// Extract data from bson
void AbstractIEFile::extractDataToWrite( const std::string& blockdata, ValuesMap& writeData )
{
    std::string impexKey, impexOrganization, nodeValue;
    std::vector<std::string> nodeValues;
    externData.clear();

    // extract json data to internal structure
    structNode->clearField();
    parseJsonToNode( blockdata, structNode );
    // Run script for operation on data values in block (if need some data preparation )
    run_block_script(block.script, structNode);

    // set up predefined data
    auto itr1 = block.defaults.begin();
    while ( itr1 != block.defaults.end() )
    {
      impexKey = itr1->first;
      impexOrganization = impexFieldOrganization( impexKey );

      // we can split second to vector if type "list" | "set"
      if( impexOrganization=="list" || impexOrganization == "set"  )
        nodeValues = regexp_split( itr1->second, " "/*format.value_token_regexp*/ );
      else
         nodeValues =  { itr1->second };
      addMapKeyValue( impexKey, nodeValues );
      itr1++;
    }

    // extract fields from structNode
    auto idom = block.matches.begin();
    while ( idom != block.matches.end() )
    {
        impexKey = idom->first;
        std::string domkey = idom->second.field; //idom->second.fieldkey;
        if( idom->second.ignore || domkey.empty() )  // skip ignored
        {   idom++; continue; }

        auto fld = structNode->field(domkey);
        if( !fld )
           {   idom++; continue; } // field is not present

        if( fld->isArray() )
        {
            auto sizes = fld->getArraySizes();
            nodeValues.resize(sizes[0]);
            fld->getArray(nodeValues);
        }
        else
         {
           if( !fld->getValue( nodeValue  ) )
             {   idom++; continue; }     //error extraction
           nodeValues = { nodeValue};
         }

        impexOrganization = impexFieldOrganization( impexKey );
        bool isArray = impexOrganization=="list" || impexOrganization == "set";

        run_convert( idom->second.convert, nodeValues );
         if( fld->elemTypeId() == ::apache::thrift::protocol::T_STRING )
          run_lua_script_string( idom->second.script, nodeValues, isArray );
         else
           run_lua_script( idom->second.script, nodeValues, isArray );

        addMapKeyValue( impexKey, nodeValues );
        idom++;
    }

    // add to results
    writeData.insert( externData.begin(), externData.end());
}


//--------------------------------------------------------------------------
// Export/import of data records to/from text file in legacy JSON/YAML/XML (foreign keywords)

StructDataIEFile::StructDataIEFile( int amode, const ThriftSchema* aschema, const std::string& alualibPath,
     const FormatStructDataFile& defData ):
           AbstractIEFile( amode, aschema, alualibPath),  renderer(defData.renderer)
{
   // set up data from  FormatKeyValueFile !!!
    label = defData.label;
    comment = defData.comment;
    file_name = defData.fname;
    Nblocks = defData.Nblocks;
    Nlines = defData.Nlines;
    block = defData.block;
    structNode = JsonDomSchema::newSchemaObject( label );

    // select file type
    if(renderer == "json" || renderer == "JSON" )
      _ftype = FileTypes::Json_;
    else  if(renderer == "yaml" || renderer == "YAML" )
            _ftype = FileTypes::Yaml_;
    else if(renderer == "xml" || renderer == "XML" )
            _ftype = FileTypes::XML_;
         else _ftype = FileTypes::Undef_;
}

// Open file to import
void StructDataIEFile::openFile( const std::string& inputFname )
{

  jsonioErrIf( _mode != modeImport  , file_name, "No export to format...");

  if( !inputFname.empty() )
    file_name = inputFname;

  // read all input array to internal
  _stream.reset( new FJsonYamlXmlArray( inputFname ) );
  _stream->Open( OpenModeTypes::ReadOnly );
}

// Close internal file
void StructDataIEFile::closeFile()
{
    if (_stream->testOpen() )
      _stream->Close();
}


// Read next bson object
bool StructDataIEFile::nextFromFile( std::string& inputjson )
{
  return _stream->LoadNext( inputjson );
}

// Read data from one block in file
bool StructDataIEFile::readBlockFromFile()
{
    std::string inputjson;
    if( !nextFromFile( inputjson ) )
       return false;

    std::shared_ptr<JsonDomFree> node(JsonDomFree::newObject());
    if( !parseJsonToNode( inputjson, node.get() ) )
      return false;

    // set data to readedData
    std::string key;
    std::vector<std::string> _value;
    auto itr = block.matches.begin();
    while ( itr != block.matches.end() )
    {
        key = itr->first;
        if( node->findToArray( key, _value ) )
         externData.insert( std::pair<std::string, std::vector<std::string>>(key, _value) );
        itr++;
    }
    return true;
}

// Read data one block from bson data
bool StructDataIEFile::readBlockFromBson( const std::string& inputjson, std::string& secondKey, int ndx )
{
    if( inputjson.empty() )
       return false;

    secondKey = "";
    std::shared_ptr<JsonDomFree> node(JsonDomFree::newObject());
    if( !parseJsonToNode( inputjson, node.get() ) )
      return false;

    // set data to readedData
    std::string key, key2;
    std::vector<std::string> _value;
    auto itr = block.matches.begin();
    while ( itr != block.matches.end() )
    {
        key2 = key = itr->first;

        if( ndx>=0)
         key2 = replace( key, "%", std::to_string(ndx).c_str());

        if( node->findToArray( key2, _value ) )
        { externData.insert( std::pair<std::string, std::vector<std::string>>(key, _value) );
          if(itr->second.field == "@key" )
          {   // convert key or get substring
              _lua.runFunc(itr->second.script, _value[0]);
              secondKey += _value[0];
          }
        } else
          {
              if(key != key2 )
               return false;
          }

        itr++;
    }
    return true;
}

/// Convert free format bson to schema defined
bool StructDataIEFile::readBlock(  const std::string& inputjson, std::string& schemajson, std::string& key, int ndx )
{
   // read data from block to readedData
    externData.clear();
    if( !readBlockFromBson(inputjson, key, ndx ) )
           return false;

   // reset default structure
      structNode->clearField();
   // write data to structNode
      setReadedData();
      run_block_script(block.script, structNode );

   // return data to json string
     printNodeToJson( schemajson, structNode );
     return true;
}


//--------------------------------------------------------------------------
// Definition of text file with key-value pair data

KeyValueIEFile::KeyValueIEFile( int amode, const ThriftSchema* aschema, const std::string& alualibPath,
           const FormatKeyValueFile& defData ):
           AbstractIEFile( amode, aschema, alualibPath),
           format(defData.format), renderer(defData.renderer)
{
   // set up data from  FormatKeyValueFile !!!
    label = defData.label;
    comment = defData.comment;
    file_name = defData.fname;
    block = defData.block;
    Nblocks = defData.Nblocks;
    Nlines = defData.Nlines;
    structNode = JsonDomSchema::newSchemaObject( label );
    str_buf = "";
    test_start = str_buf.begin();
    test_end = str_buf.end();

}

// Open file to import
void KeyValueIEFile::openFile( const std::string& inputFname )
{
    use_string_stream = false;

    if( !inputFname.empty() )
        file_name = inputFname;

    // open  file
    if (_fstream.is_open())
        _fstream.close();

    if( _mode == modeImport )
        _fstream.open(file_name,  std::fstream::in );
    else
        _fstream.open(file_name,  std::fstream::out|std::fstream::app );

    jsonioErrIf( !_fstream.good() , file_name, "Fileopen error...");
}

// Close internal file
void KeyValueIEFile::closeFile()
{
    // close file
    if (_fstream.is_open())
        _fstream.close();
}

void KeyValueIEFile::loadString(const std::string &inputData)
{
    use_string_stream = true;

    if (_fstream.is_open())
        _fstream.close();

    if( _mode == modeImport )
        _stringstream.str(inputData );
    else
        _stringstream.str("");
    jsonioErrIf( !_stringstream.good(), "loadString", "Loading String error...");

}

std::string KeyValueIEFile::getString()
{
    if( use_string_stream)
        return  _stringstream.str();
    jsonioErr( "getString", "Working with file stream.");
}


/// Read next block of data from file
bool  KeyValueIEFile::readNextBlockfromFile()
{
    // clear analyzed block
    str_buf = std::string(test_start, test_end);
    // delete spaces
    if( !str_buf.empty() )
    { std::string::size_type pos1 = str_buf.find_first_not_of(" \t\r\n");
      if( pos1 != std::string::npos )
        str_buf = str_buf.substr(pos1);
      else
        str_buf = "";
    }
    test_start = str_buf.begin();
    test_end = str_buf.end();

    if( get_istream().eof() || !get_istream().good()  )
        return false;

    //let's say read by 2048 char block
    char buffer[2049];
    memset(buffer,'\0', 2049 );
    // read next block
    get_istream().read( buffer, 2048 );
    str_buf += std::string(buffer);
    test_start = str_buf.begin();
    test_end = str_buf.end();
    //cout << endl << "!!!!" << _str_buf.c_str() << "\n<<<" << endl;

    return true;
}

/// Skip comments and blanks
void KeyValueIEFile::skipComments()
{
    if( str_buf.empty() )
      return;

    std::string::size_type posb;
    std::vector<std::string> mach_str;

    do{
        if( !mach_str.empty() )
          insertKeyValue( "comment", mach_str );

        posb = str_buf.find_first_not_of(" \t\r\n",test_start-str_buf.begin() );
        if( posb != std::string::npos )
          test_start = str_buf.begin()+posb;
        else
          test_start = test_end;

        if( test_start == test_end )
              break;
    } while( !format.comment_regexp.empty() && readRegexp( format.comment_regexp, mach_str ) );

}

///  Test/read next regexp
///  \param  re_str - string with Regular Expression
///  \param  mach_str - list of matched strings
///  \return true if matched Expression
bool KeyValueIEFile::readRegexp( const std::string& re_str, std::vector<std::string>& mach_str )
{
 try
  {
   mach_str.clear();

   if( test_start == test_end )
    return false;

   rgx::match_results<std::string::const_iterator> what;
   //rgx::match_flag_type flags = rgx::match_continuous| rgx::match_partial;
   rgx::regex_constants::match_flag_type flags = rgx::regex_constants::match_continuous;//| rgx::regex_constants::match_partial;
   const rgx::regex expression( re_str.c_str() );

   while(rgx::regex_search(test_start, test_end, what, expression, flags))
   {
      // match only partial
      if( !what[0].matched  )
      {
          if( !readNextBlockfromFile() )
            return false; // end of file
      }
      else
      { // what[0] contains the whole string
        // what[>0] contains the tokens.

        for( uint ii=1; ii < what.size(); ii++ )
        { //cout <<  what[ii] << endl;
          mach_str.push_back(what[ii]);
        }
        // update search position:
        test_start = what[0].second;
        return true;
      }
   }

  }
   catch (rgx::regex_error &e)
    {
      std::cout << "what: " << e.what() << "; code: " << parseCode(e.code()) << std::endl;
      jsonioErr("Error in Regular Expression", e.what(), re_str);
    }
  return false;
}

///  Only Test next regexp
///  \param  re_str - string with Regular Expression
///  \return true if matched Expression
bool KeyValueIEFile::testRegexp( const std::string& re_str )
{
 try
  {
   //if( test_start == test_end )
   // return false;

   rgx::match_results<std::string::const_iterator> what;
   //rgx::match_flag_type flags = rgx::match_continuous| rgx::match_partial;
   rgx::regex_constants::match_flag_type flags = rgx::regex_constants::match_continuous;//| rgx::regex_constants::match_partial;
   const rgx::regex expression( re_str.c_str() );
   while(rgx::regex_search(test_start, test_end, what, expression, flags))
   {
      // match only partial
      if( !what[0].matched  )
      {
        if( !readNextBlockfromFile())
            return false; // end of file
      }
      else
        return true;
   }
  }
   catch (rgx::regex_error &e)
    {
      std::cout << "what: " << e.what() << "; code: " << parseCode(e.code()) << std::endl;
      jsonioErr("Error in Regular Expression", e.what(), re_str);
    }
  return false;
}

/// Read header of block
bool KeyValueIEFile::readStartBlock()
{
  // read next block of data from file
  readNextBlockfromFile();

  // skip comments and blanks
  skipComments();

  // nothing to test
  if( str_buf.empty() )
    return false;

  // no head condition
  if( format.head_regexp.empty() )
    return true;

  // read header of block
  std::vector<std::string> mach_str;
  bool ret = readRegexp( format.head_regexp, mach_str );
  if( ret )
    for(uint ii=0; ii<mach_str.size(); ii++ )
    {
      std::string _key = "head"+std::to_string(ii);
      addKeyValue( _key, mach_str[ii] );
    }

  return ret;
}

/// Read&test end block
bool KeyValueIEFile::isEndBlock()
{
    bool ret;

    // skip comments and blanks
    skipComments();

    // no end condition (test next head condition or end of file )
    if( format.end_regexp.empty() )
    {
        if( test_start == test_end )
         ret = true;
        else
         ret = testRegexp(  format.head_regexp );
    } else
      { // read end of block
        std::vector<std::string> mach_str;
        ret = readRegexp(  format.end_regexp, mach_str );
        if( ret )
          for(uint ii=0; ii<mach_str.size(); ii++ )
          {
            std::string _key = "end"+std::to_string(ii);
            addKeyValue( _key, mach_str[ii] );
          }
    }
   return ret;
}

/// Read value untill next key or end of block
std::string KeyValueIEFile::readAllValue()
{
  std::string value = "";
  size_t posb;

  auto pos = test_start;
  while( pos != test_end )
  {
      // test next key;
      if( testRegexp(  format.key_regexp ) )
       break;

      // test next end of block
      if( format.end_regexp.empty() )
      {   if( testRegexp(  format.head_regexp ) )
            break;
      } else
          if( testRegexp(  format.end_regexp ) )
             break;

      if( format.value_next.empty() )
      {
        pos+=1;   // only next symbol
      }
      else
        {
          posb =  test_start-str_buf.begin();
          posb = str_buf.find( format.value_next, posb );
          if( posb != std::string::npos )
            pos = str_buf.begin()+posb+format.value_next.length();
          else
            pos = test_end;
        }
      value += std::string( test_start, pos );
      test_start = pos;

      if( pos == test_end )
      {  if( !readNextBlockfromFile() )
           break;
         pos = test_start;
      }
  }
  posb = value.rfind(format.value_next);
  return value.substr(0,posb );
}

/// Read key value pair
bool KeyValueIEFile::readKeyValuePair()
{
    std::string key, value="";
    std::vector<std::string> mach_str;

    // skip comments and blanks
    skipComments();

    // get key
    if( !readRegexp(  format.key_regexp, mach_str ))
      return false;
    key =mach_str[0];

    // skip comments and blanks
    skipComments();

    // get value
    if( !format.value_regexp.empty() )
    {
        if( readRegexp(  format.value_regexp, mach_str ))
           value =mach_str[0];
    }
    else  // value untill end of block or key
    {
       value = readAllValue();
    }

    strip_all(value);
    addKeyValue( key, value );
    return true;
}

/// Inser key value paies to readed list
void KeyValueIEFile::insertKeyValue( const std::string& _key, const std::vector<std::string>& vec )
{
    auto itr = externData.find(_key);
    if( itr != externData.end() )
    {    for( uint ii=0; ii<vec.size(); ii++ )
           itr->second.push_back(vec[ii]);
    }
    else
     externData.insert( std::pair<std::string, std::vector<std::string>>(_key, vec) );

}

/// Add key value data to Readed Key, Value pairs
void KeyValueIEFile::addKeyValue( const std::string& _key, const std::string& _value )
{
    std::vector<std::string> vec;
    vec.push_back(_value);

    auto  organization = impexFieldOrganization( _key );

    //  here we test if key is array type in  block.pairs type
    //  we split _value to vector use format.value_part_regexp
    if( organization == "list" ||   organization == "set" ||
        organization == "map" ||     organization == "group"  )
      vec = regexp_split(_value, format.value_token_regexp );

    if( organization == "group"  )
    {
          for( uint ii=0; ii<vec.size(); ii++)
           insertKeyValue( _key+std::to_string(ii), { vec[ii] } );
          return;
    }
    insertKeyValue( _key, vec );
 }


/// Read data from one block in file
bool KeyValueIEFile::readBlockFromFile( )
{
  if( !readStartBlock() )
      return false;

  int ii=0;
  while( !isEndBlock() )
  {
      if( format.Ndata>0 && ii > format.Ndata )
        return true;
      if( !readKeyValuePair() )
         jsonioErr("ReadKeyValuePair -error ",
                   std::string(test_start, min(test_start+100, test_end)));
      ii++;
  }
  /* test output
  auto it = externData.begin();
  while( it != externData.end() )
  {
   cout << " key " << it->first ;
   for(uint ii=0; ii< it->second.size(); ii++ )
    cout << "         "  << it->second[ii].c_str() << endl;
   it++;
  }*/
  return true;
}


// Convert free format data to schema defined
bool KeyValueIEFile::initBlock(   std::string& blockdata,  const ValuesMap& areadedData )
{
  // read data from block to readedData
    externData = areadedData;
  // reset default structure
    structNode->clearField();
  // write data to structNode
     setReadedData();
  // Run script for operation on data values in block
     run_block_script(block.script, structNode );

  // return data to json string
     printNodeToJson( blockdata, structNode );
     return true;
}

// export

bool KeyValueIEFile::writeBlockToFile( const ValuesMap& writeData_ )
{
    externData = writeData_;
    assembleGroups( externData  );
    std::vector<std::string> impexValues;
    std::string impexOrganization, impexType;
    uint impexSize;

    // write head
    std::vector<std::string> tokens = regexp_extract( format.head_regexp, "%head\\d+");
    std::string headstr = format.head_regexp;
    for( auto token: tokens )
    {
        impexValues = extractValue( token.substr(1), externData );
        if( impexValues.size() > 0 )
             headstr = regexp_replace( headstr, token, impexValues[0] );
    }
    get_ostream() << headstr;

    //build order list
    auto orderFields = format.key_order;
    if( orderFields.empty() )
      for( auto item: externData )
      {
        auto impexKey = item.first;
        if( impexKey.find("head") == std::string::npos && impexKey.find("end") == std::string::npos)
         orderFields.push_back(impexKey);
      }

    // write matches (use orderFields )
    for( auto impexKey: orderFields )
    {
      auto idom = externData.find(impexKey);
      if( idom == externData.end() ) //not exist
         continue;
      impexValues = idom->second;
      if( impexValues.empty() )
         continue;

      get_ostream() << regexp_replace( format.key_regexp, "%key", impexKey );

      impexOrganization = impexFieldOrganization( impexKey );
      if( impexOrganization=="list" || impexOrganization == "set" ||
              impexOrganization == "group" )
       impexSize = impexValues.size();
      else
       impexSize = 1;

      impexType = impexFieldType( impexKey );
      for( uint ii=0; ii< impexSize; ii++  )
       if( !format.strvalue_exp.empty()  && impexType == "string" )
         get_ostream() << regexp_replace( format.strvalue_exp, "%value", impexValues[ii] ) << format.value_token_regexp;
       else
         get_ostream() << regexp_replace( format.value_regexp, "%value", impexValues[ii] ) << format.value_token_regexp;

      get_ostream() << format.value_next;
    }

    // write end of block
    tokens = regexp_extract( format.end_regexp, "%end\\d+");
    std::string endstr = format.end_regexp;
    for( auto token: tokens )
    {
        impexValues = extractValue( token.substr(1), externData );
        if( impexValues.size() > 0 )
             endstr = regexp_replace( endstr, token, impexValues[0] );
    }
    get_ostream() << endstr;
   return true;
}



//-----------------------------------------------------------------------

TableIEFile::TableIEFile( int amode, const ThriftSchema* aschema, const std::string& alualibPath,
                          const FormatTableFile& defData ):
    AbstractIEFile( amode, aschema, alualibPath),
    format(defData.format), renderer(defData.renderer)
{
    // set up data from  FormatKeyValueFile !!!
    label = defData.label;
    comment = defData.comment;
    file_name = defData.fname;
    block = defData.block;
    Nblocks = defData.Nblocks;
    Nlines = defData.Nlines;
    structNode = JsonDomSchema::newSchemaObject( label );
}

// Open file to import
void TableIEFile::openFile( const std::string& inputFname )
{
    use_string_stream = false;

    if( !inputFname.empty() )
        file_name = inputFname;

    // open  file
    if (_fstream.is_open())
        _fstream.close();

    if( _mode == modeImport )
        _fstream.open(file_name,  std::fstream::in );
    else
        _fstream.open(file_name,  std::fstream::out|std::fstream::app );
    jsonioErrIf( !_fstream.good() , file_name, "Fileopen error...");

    if( _mode == modeImport )
        readHeaderFromFile();
    ///  else
    ///    writeHeaderToFile();

}

void TableIEFile::loadString(const std::string &inputData)
{
    use_string_stream = true;

    // close  file
    if (_fstream.is_open())
        _fstream.close();

    if( _mode == modeImport )
        _stringstream.str(inputData );
    else
        _stringstream.str("");
    jsonioErrIf( !_stringstream.good(), "loadString", "Loading String error...");

    if( _mode == modeImport )
        readHeaderFromFile();
    ///  else
    ///    writeHeaderToFile();

}

std::string TableIEFile::getString()
{
    if( use_string_stream )
        return  _stringstream.str();
    jsonioErr( "getString", "Working with file stream.");
}

// Close internal file
void TableIEFile::closeFile()
{
    if (_fstream.is_open())
    _fstream.close();
}

/// Read data from one block in file
bool TableIEFile::readBlockFromFile( )
{
  std::string valuesRow = readNextRow();
  if( valuesRow.empty() )
      return false;

 if( !format.colsizes.empty() )
      splitFixedSize( valuesRow );
  else
      if( !format.value_regexp.empty() )
          splitRegExpr( valuesRow );
      else
          splitDelimiter( valuesRow );

  /* test output
  auto it = externData.begin();
  while( it != externData.end() )
  {
   cout << " key " << it->first << ":" ;
   for(uint ii=0; ii< it->second.size(); ii++ )
       cout << " \""  << it->second[ii] << "\" ";
   it++;
  }
  cout << "------------\n";
  */
  return true;
}


/// Read data from one block in file
bool TableIEFile::readHeaderFromFile( )
{
  if( format.Nhrows <= 0)  // no headers
    return  true;

 std:: string valuesRow = readNextRow();
  if( valuesRow.empty() )
      return false;

  if( !format.colsizes.empty() )
      splitFixedSize( valuesRow, true );
  else
      //if( !format.value_regexp.empty() )
      //    splitRegExpr( valuesRow, true );
      //else
          splitDelimiter( valuesRow, true );

  if( format.Nhcols == 0 )
      format.Nhcols = format.headers.size();

  // skip rows
  for( int ii=1; ii<format.Nhrows; ii++)
    valuesRow = readNextRow();

  return true;
}

void TableIEFile::skipComments( std::string& row )
{
  try{
    rgx::regex re(format.comment_regexp);
    row = regex_replace(row, re, "");
    }
     catch (rgx::regex_error &e)
      {
        std::cout << "what: " << e.what() << "; code: " << parseCode(e.code()) << std::endl;
        jsonioErr("Error in Regular Expression", e.what(), format.comment_regexp);
      }
}

std::string TableIEFile::read_next_data( )
{
    char buffer[2049];
    memset(buffer,'\0', 2049 );
    get_istream().getline( buffer, 2048, format.rowend[0] );
    std::string nextrow = std::string(buffer);
    skipComments( nextrow );
    if( format.row_size > 0 && !nextrow.empty() )
        nextrow.resize(format.row_size,' ');
    return nextrow;
}


std::string TableIEFile::readNextRow( )
{
    std::string next_all_row, nextrow;

    if( !format.row_header_regexp.empty() )
    {
        next_all_row += std::move(next_row_data);
        next_row_data = "";
        while( !get_istream().eof() && get_istream().good()  )
        {
            nextrow = read_next_data();
            if( nextrow.empty())
                continue;

            if( regexp_test(nextrow, format.row_header_regexp) && !next_all_row.empty() ) {
                next_row_data = nextrow+format.rowend[0];
                break;
            }
            else {
                next_all_row += nextrow+format.rowend[0];
            }
        }
    }
    else
    {
        int ii= std::max( format.rows_one_block, 1);
        while(  ii > 0 )
        {
            if( get_istream().eof() || !get_istream().good()  )
                break;
            // read next block
            nextrow = read_next_data();
            if( !nextrow.empty() ) // skip empty or comment rows
            {
                next_all_row += nextrow;
                ii--;
            }
        }
    }
    return next_all_row;
}

void TableIEFile::addValue( size_t col, const std::string& _value, bool toheader )
{
    if( toheader )
    {
        if( format.headers.size() < col+1 )
            format.headers.resize(col+1);
        format.headers[col] = _value;
        return;
    }

    if( _value.empty() ) // no add empty
      return;

    std::vector<std::string> vec;
    vec.push_back(_value);

    std::string key;
    if( col < format.headers.size() )
        key = format.headers[col];
    if( key.empty())
        key = "clm"+std::to_string(col);

    externData.insert( std::pair<std::string, std::vector<std::string>>(key, vec) );
}

void TableIEFile::splitFixedSize( const std::string& readedrow, bool toheader )
{
  std::string value, row = readedrow;
  auto colsizes = format.colsizes;
  while( static_cast<int>(colsizes.size()) < format.Nhcols )
    colsizes.push_back(colsizes.back());

  for( size_t ii=0; ii<colsizes.size(); ii++)
  {
      if( row.empty())
        break;
      value = row.substr(0, colsizes[ii]);
      row = row.substr( std::min<size_t>( colsizes[ii], row.size()));
      strip_all(value);
      addValue( ii, value, toheader );
  }
}

void TableIEFile::splitRegExpr( const std::string& readedrow, bool toheader )
{
  uint ii=0;
  std::string value, row = readedrow;
  auto rgex = format.value_regexp;
  while( static_cast<int>(rgex.size()) < format.Nhcols )
    rgex.push_back(rgex[0]);

 try{
    for( ii=0; ii<rgex.size(); ii++)
    {
      rgx::regex re(rgex[ii]);
      rgx::smatch sm;
      if(rgx::regex_search(row, sm, re))
      {
          for (rgx::smatch::iterator it = sm.begin(); it!=sm.end(); ++it)
          {
              value = *it;
          }
        //value = sm.str();
        row = sm.suffix();
        addValue( ii, value, toheader);
      }
      else
       addValue( ii, "", toheader);
    }
  }
   catch (rgx::regex_error &e)
    {
      std::cout << "what: " << e.what() << "; code: " << parseCode(e.code()) << std::endl;
      jsonioErr("Error in Regular Expression", e.what(), rgex[ii] );
    }

}

uint TableIEFile::extractQuoted( const std::string& row, std::string& value )
{
     rgx::regex re("\"([^\"]+)\"");
     rgx::smatch sm;
     if(rgx::regex_search(row, sm, re))
     {  value = sm.str();
        return value.length()+2;
     }
     return 0;
}

void TableIEFile::splitDelimiter( const std::string& readedrow, bool toheader )
{
  uint ii=0;
  std::string value, row = readedrow;

  std::string::size_type start = 0;
  auto pos = row.find_first_of(format.colends, start);
  while(pos != std::string::npos)
  {
      if( !format.usemore || pos != start) // ignore empty tokens
      {
        if(format.usequotes && row[start]== '\"' )
         {
           auto dlt = extractQuoted( std::string( row, start), value );
           if( dlt )
            pos = start + dlt;
           else
            value = std::string(row, start, pos - start);
         }
         else
           value = std::string(row, start, pos - start);

        strip(value);
        addValue( ii++, value, toheader);
      }
      start = pos + 1;
      pos = row.find_first_of(format.colends, start);
  }
  if(start < row.length()) // ignore trailing delimiter
  {
      value =std::string (row, start, row.length() - start);
      strip(value);
      addValue( ii, value, toheader);
  }
}

bool TableIEFile::writeBlockToFile( const ValuesMap& writeData )
{
   std::vector<std::string> values;
   auto colsizes = format.colsizes;
   auto rgex = format.value_regexp;
   if( !format.colsizes.empty() )
    {
       while( colsizes.size() < format.headers.size() )
         colsizes.push_back(colsizes[0]);
    }
   else
       if( !format.value_regexp.empty() )
       {
         while( rgex.size() < format.headers.size() )
             rgex.push_back(rgex[0]);
       }

   size_t ii=0;
   for( auto colhead: format.headers )
   {
       values = extractValue( colhead, writeData );
       if( values.empty() )
          values.push_back(" ");

       if( !colsizes.empty() )
          {
            values[0].resize(colsizes[ii++], ' ');
           get_ostream() << values[0];
          }
       else
           if( !rgex.empty() )
              get_ostream() << regexp_replace( rgex[ii++], "%value", values[0] );
           else
              get_ostream() << values[0] << format.colends;
   }
   get_ostream() << format.rowend;
   return true;
}


//-------------------------------------------------------------------------------
// lib functions

std::string parseCode(/*regex_constants::error_type*/ int etype)
{
  switch (etype)
  {
    case rgx::regex_constants::error_collate:
        return "error_collate: invalid collating element request";
    case rgx::regex_constants::error_ctype:
        return "error_ctype: invalid character class";
    case rgx::regex_constants::error_escape:
        return "error_escape: invalid escape character or trailing escape";
    case rgx::regex_constants::error_backref:
        return "error_backref: invalid back reference";
    case rgx::regex_constants::error_brack:
        return "error_brack: mismatched bracket([ or ])";
    case rgx::regex_constants::error_paren:
        return "error_paren: mismatched parentheses(( or ))";
    case rgx::regex_constants::error_brace:
        return "error_brace: mismatched brace({ or })";
    case rgx::regex_constants::error_badbrace:
        return "error_badbrace: invalid range inside a { }";
    case rgx::regex_constants::error_range:
        return "erro_range: invalid character range(e.g., [z-a])";
    case rgx::regex_constants::error_space:
        return "error_space: insufficient memory to handle this regular expression";
    case rgx::regex_constants::error_badrepeat:
        return "error_badrepeat: a repetition character (*, ?, +, or {) was not preceded by a valid regular expression";
    case rgx::regex_constants::error_complexity:
        return "error_complexity: the requested match is too complex";
    case rgx::regex_constants::error_stack:
        return "error_stack: insufficient memory to evaluate a match";
    default:
        return "";
  }
}

/**


void myFunction(std::istream &is) {
    std::string oneline;
    while (getline(is, oneline))
       process(oneline);
}

if (file) {
    std::ifstream inFile(filename);
    myFunction(inFile);
}
else {
    std::istringstream fromMemory(...);
    myFunction(fromMemory);
}



*/

//  Function that can be used to split text using regexp
std::vector<std::string> regexp_split(const std::string& str, std::string rgx_str)
{
  std::vector<std::string> lst;

  rgx::regex rgx(rgx_str);

  rgx::sregex_token_iterator iter(str.begin(), str.end(), rgx, -1);
  rgx::sregex_token_iterator end;

  while (iter != end)
  {
    lst.push_back(*iter);
    strip_all(lst.back(), " \t\n");
    ++iter;
  }

  return lst;
}

//  Function that can be used to extract tokens using regexp
std::vector<std::string> regexp_extract(const std::string& str, std::string rgx_str)
{
  std::vector<std::string> lst;

  rgx::regex rgx(rgx_str);

  rgx::sregex_token_iterator iter(str.begin(), str.end(), rgx, 0);
  rgx::sregex_token_iterator end;

  while (iter != end)
  {
    lst.push_back(*iter);
    strip_all(lst.back(), " \t\n");
    ++iter;
  }

  return lst;
}


//  Function that can be used to replase text using regexp
std::string regexp_replace(const std::string& instr, const std::string& rgx_str, const std::string& replacement )
{
   rgx::regex re(rgx_str);
   std::string output_str = rgx::regex_replace(instr, re, replacement);
   return output_str;
}


} // namespace jsonio
