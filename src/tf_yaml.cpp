//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file v_yaml.cpp
/// Implementation of functions for data exchange to/from YAML format
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <fstream>
#include <iostream>
#include <cassert>
#include <stack>
#include "yaml-cpp/yaml.h"
#include "yaml-cpp/eventhandler.h"
#include "jsonimpex/tf_yaml.h"
using namespace std;


using namespace YAML;

namespace jsonio {


namespace ParserYAML {

/// The BsonHandler class provides support for dynamically generating
/// JsonNode data from yaml parser
class NodeHandler: public YAML::EventHandler
{

 public:

  string to_string();

  NodeHandler( JsonDom* object );

  virtual void OnDocumentStart(const YAML::Mark& mark);
  virtual void OnDocumentEnd();

  virtual void OnNull(const YAML::Mark& mark, YAML::anchor_t anchor);
  virtual void OnAlias(const YAML::Mark& mark, YAML::anchor_t anchor);
  virtual void OnScalar(const YAML::Mark& mark, const std::string& tag,
                        YAML::anchor_t anchor, const std::string& value);

  virtual void OnSequenceStart(const YAML::Mark& mark, const std::string& tag,
                               YAML::anchor_t anchor, YAML::EmitterStyle::value style);
  virtual void OnSequenceEnd();

  virtual void OnMapStart(const YAML::Mark& mark, const std::string& tag,
                          YAML::anchor_t anchor, YAML::EmitterStyle::value style);
  virtual void OnMapEnd();

 private:

  void BeginNode();
  //void EmitProps(const std::string& tag, YAML::anchor_t anchor);

 private:
  JsonDom* m_object;

  enum   STATE_ { WaitingForSequenceEntry, WaitingForKey, WaitingForValue, };

  struct State {
    string key;
    int    ndx;
    STATE_ state;

    State( STATE_ st):
        key(""),ndx(0),state(st)
    {}
  };

  std::stack<State> m_stateStack;
  std::stack<JsonDom*> m_objectStack;
};

/// The BsonHandler class provides support for dynamically generating
/// json data from yaml parser
class JsonHandler1: public YAML::EventHandler
{

 void addHead(const string& key );
 void addScalar(const string&  key, const string& value );
 void shift()
 {
   for(int temp = 0; temp < m_depth; temp++)
        m_os << "     ";
 }

 public:

  string to_string();

  JsonHandler1(stringstream& os_);

  virtual void OnDocumentStart(const YAML::Mark& mark);
  virtual void OnDocumentEnd();

  virtual void OnNull(const YAML::Mark& mark, YAML::anchor_t anchor);
  virtual void OnAlias(const YAML::Mark& mark, YAML::anchor_t anchor);
  virtual void OnScalar(const YAML::Mark& mark, const std::string& tag,
                        YAML::anchor_t anchor, const std::string& value);

  virtual void OnSequenceStart(const YAML::Mark& mark, const std::string& tag,
                               YAML::anchor_t anchor, YAML::EmitterStyle::value style);
  virtual void OnSequenceEnd();

  virtual void OnMapStart(const YAML::Mark& mark, const std::string& tag,
                          YAML::anchor_t anchor, YAML::EmitterStyle::value style);
  virtual void OnMapEnd();

 private:

  void BeginNode();
  //void EmitProps(const std::string& tag, YAML::anchor_t anchor);

 private:
  stringstream& m_os;
  int m_depth =0;
  bool m_first= true;

  enum   STATE_ { WaitingForSequenceEntry, WaitingForKey, WaitingForValue, };

  struct State {
    string key;
    int    ndx;
    STATE_ state;

    State( STATE_ st):
        key(""),ndx(0),state(st)
    {}
  };

  std::stack<State> m_stateStack;
};


void object_emitter( Emitter& out, const JsonDom* object );

// Parse one YAML object from string to json structure
string YAML2Json1( const string& currentYAML )
{
  stringstream jsonstriam;

 try{
     stringstream stream(currentYAML);
     YAML::Parser parser(stream);
     JsonHandler1 builder(jsonstriam);
     parser.HandleNextDocument(builder);
     //cout << builder.to_string() << endl;
   }
   catch(YAML::Exception& e) {
      cout << "YAML2Json " << e.what() << endl;
      jsonioErr( "YAML2Json",  e.what() );
   }
  return jsonstriam.str();
 }


void printNodeToYAML(fstream& fout, const JsonDom *object)
{
    Emitter out;
    int objtype = object->getType();
    if(objtype == JSON_OBJECT)
       out << BeginMap;
    else
        if(objtype == JSON_ARRAY)
           out << BeginSeq;
        else
           return;
    object_emitter( out, object );
    if(objtype == JSON_OBJECT)
       out << EndMap;
    else
       out << EndSeq;
    fout << out.c_str();
}

void printNodeToYAML(string& fout, const JsonDom* object)
{
    Emitter out;
    int objtype = object->getType();
    if(objtype == JSON_OBJECT)
       out << BeginMap;
    else
        if(objtype == JSON_ARRAY)
           out << BeginSeq;
        else
           return;
    object_emitter( out, object );
    if(objtype == JSON_OBJECT)
       out << EndMap;
    else
       out << EndSeq;
    fout = string( out.c_str());
 //   cout << fout;
}


// Parse one YAML object from string to bson structure
bool parseYAMLToNode( fstream& fin, JsonDom* object )
{
 try{
     YAML::Parser parser(fin);
     NodeHandler builder(object);
     return parser.HandleNextDocument(builder);
   }
   catch(YAML::Exception& e) {
      cout << "parseYAMLToBson " << e.what() << endl;
      jsonioErr( "parseYAMLToBson",  e.what() );
   }
  //return false;
 }

// Parse one YAML object from string to bson structure
bool parseYAMLToNode( const string& currentYAML, JsonDom* object )
{
 try{
     stringstream stream(currentYAML);
     YAML::Parser parser(stream);
     NodeHandler builder(object);
     return parser.HandleNextDocument(builder);
     //cout << builder.to_string() << endl;
   }
   catch(YAML::Exception& e) {
      cout << "parseYAMLToBson " << e.what() << endl;
      jsonioErr( "parseYAMLToBson",  e.what() );
   }
   //return true;
 }

void object_emitter( Emitter& out, const JsonDom* object )
{
    int objtype = object->getType();
    size_t objsize = object->getChildrenCount();

    for( size_t ii=0; ii<objsize; ii++ )
    {
       auto childobj = object->getChild( ii);
       switch( objtype )
       {
        case JSON_OBJECT:
           out << Key << childobj->getKey();
           out << Value;
           break;
        default:
           break;
       }

       switch( childobj->getType() )
        {
          // impotant datatypes
          case JSON_NULL:
                 out << _Null();
                 break;
          case JSON_BOOL:
          case JSON_INT:
          case JSON_DOUBLE:
          case JSON_STRING:
                 out <<  childobj->toString();
                 break;
          // main constructions
          case JSON_OBJECT:
                 out << BeginMap;
                 object_emitter( out, childobj );
                 out << EndMap;
                 break;
          case JSON_ARRAY:
                 out << BeginSeq;
                 object_emitter(out, childobj );
                 out << EndSeq;
                 break;
          default:
                 out  << "can't print type : " << childobj->getType();
        }
    }
}

// NodeHandler------------------------------------------------

string NodeHandler::to_string()
{
  string yamlstr;
  ParserYAML::printNodeToYAML( yamlstr, m_object);
  return yamlstr;
}

NodeHandler::NodeHandler(JsonDom* object) : m_object(object)
{
   m_objectStack.push(m_object);
}

void NodeHandler::OnDocumentStart(const Mark&) {}

void NodeHandler::OnDocumentEnd() {}

void NodeHandler::OnNull(const Mark&, anchor_t /*anchor*/)
{
  assert(m_stateStack.top().state == WaitingForValue );
  m_objectStack.top()->appendNull( m_stateStack.top().key );
  BeginNode();
}

void NodeHandler::OnAlias(const Mark&, anchor_t /*anchor*/) {}

void NodeHandler::OnScalar(const Mark&, const std::string& /*tag*/,
                              anchor_t /*anchor*/, const std::string& value)
{
   switch (m_stateStack.top().state)
   {
     case WaitingForSequenceEntry:
        { string key = std::to_string(m_stateStack.top().ndx++);
          m_objectStack.top()->appendScalar( key, value );
          break;
        }
     case WaitingForKey:
        m_stateStack.top().key = value;
        m_stateStack.top().state = WaitingForValue;
        break;
     case WaitingForValue:
        string key =  m_stateStack.top().key;
        m_objectStack.top()->appendScalar( key, value );
        m_stateStack.top().key = "";
        m_stateStack.top().state = WaitingForKey;
        break;
    }
}

void NodeHandler::OnSequenceStart(const Mark&, const std::string& /*tag*/,
            anchor_t /*anchor*/, EmitterStyle::value /*style*/)
{
  if (!m_stateStack.empty())
  {
     string key;
     if( m_stateStack.top().state == WaitingForSequenceEntry )
        key = std::to_string(m_stateStack.top().ndx++);
     else
        key =  m_stateStack.top().key;
     auto arr = m_objectStack.top()->appendArray( key );
     m_objectStack.push(arr);
  }
  else
  {

  }
  BeginNode();
  m_stateStack.push(State(WaitingForSequenceEntry));
}

void NodeHandler::OnSequenceEnd()
{
  assert(m_stateStack.top().state == WaitingForSequenceEntry );
  m_stateStack.pop();
  if (!m_stateStack.empty())
     m_objectStack.pop();
}

void NodeHandler::OnMapStart(const Mark&, const std::string& /*tag*/,
                                anchor_t /*anchor*/, EmitterStyle::value /*style*/)
{
  if (!m_stateStack.empty() )
  {
    string key;
    if( m_stateStack.top().state == WaitingForSequenceEntry )
      key = std::to_string(m_stateStack.top().ndx++);
    else
      key =  m_stateStack.top().key;
    auto obj = m_objectStack.top()->appendObject( key );
    m_objectStack.push(obj);
  }
  BeginNode();
  m_stateStack.push(State(WaitingForKey));
}

void NodeHandler::OnMapEnd() {
 m_stateStack.pop();
 if (!m_stateStack.empty() )
     m_objectStack.pop();
  //assert(m_stateStack.top().state == WaitingForKey);
}

void NodeHandler::BeginNode()
{
  if (m_stateStack.empty())
    return;

  switch (m_stateStack.top().state) {
    case WaitingForKey:
      m_stateStack.top().state = WaitingForValue;
      break;
    case WaitingForValue:
      m_stateStack.top().state = WaitingForKey;
      break;
    default:
      break;
  }
}

// JsonHandler------------------------------------------------

string JsonHandler1::to_string()
{
  return m_os.str();
}

JsonHandler1::JsonHandler1(stringstream& os_) : m_os(os_) {}

void JsonHandler1::OnDocumentStart(const Mark&) {}

void JsonHandler1::OnDocumentEnd() {}

void JsonHandler1::OnNull(const Mark&, anchor_t /*anchor*/)
{
  assert(m_stateStack.top().state == WaitingForValue );
  addScalar(m_stateStack.top().key.c_str(), "null" );
  BeginNode();
}

void JsonHandler1::OnAlias(const Mark&, anchor_t /*anchor*/) {}

void JsonHandler1::OnScalar(const Mark&, const std::string& /*tag*/,
                              anchor_t /*anchor*/, const std::string& value)
{
   switch (m_stateStack.top().state)
   {
     case WaitingForSequenceEntry:
        { m_stateStack.top().ndx++;
          addScalar( "", value );
          break;
        }
     case WaitingForKey:
        m_stateStack.top().key = value;
        m_stateStack.top().state = WaitingForValue;
        break;
     case WaitingForValue:
        string key =  m_stateStack.top().key;
        addScalar( key.c_str(), value );
        m_stateStack.top().key = "";
        m_stateStack.top().state = WaitingForKey;
        break;
    }
}

void JsonHandler1::OnSequenceStart(const Mark&, const std::string& /*tag*/,
            anchor_t /*anchor*/, EmitterStyle::value /*style*/)
{
  string key="";
  if (!m_stateStack.empty())
  {
     if( m_stateStack.top().state == WaitingForSequenceEntry )
        m_stateStack.top().ndx++;
     else
        key =  m_stateStack.top().key;
  }
  addHead( key );
  m_os << "[\n";
  m_depth++;
  m_first = true;
  BeginNode();
  m_stateStack.push(State(WaitingForSequenceEntry));
}

void JsonHandler1::OnSequenceEnd()
{
  assert(m_stateStack.top().state == WaitingForSequenceEntry );
  m_stateStack.pop();
  m_depth--;
  m_os << "\n";
  shift();
  m_os << "]";
}

void JsonHandler1::OnMapStart(const Mark&, const std::string& /*tag*/,
                                anchor_t /*anchor*/, EmitterStyle::value /*style*/)
{
  string key="";
  if (!m_stateStack.empty() )
  {
    if( m_stateStack.top().state == WaitingForSequenceEntry )
      m_stateStack.top().ndx++;
    else
      key =  m_stateStack.top().key;
  }
  addHead( key );
  m_os << "{\n";
  m_depth++;
  m_first = true;
  BeginNode();
  m_stateStack.push(State(WaitingForKey));
}

void JsonHandler1::OnMapEnd() {
 m_stateStack.pop();
 m_depth--;
 m_os << "\n";
 shift();
 m_os << "}";
}

void JsonHandler1::BeginNode()
{
  if (m_stateStack.empty())
    return;

  switch (m_stateStack.top().state) {
    case WaitingForKey:
      m_stateStack.top().state = WaitingForValue;
      break;
    case WaitingForValue:
      m_stateStack.top().state = WaitingForKey;
      break;
    default:
      break;
  }
}

void JsonHandler1::addHead(const string& key )
{
    if(!m_first )
     m_os <<  ",\n";
    else
     m_first = false;

    shift();

    if( !key.empty())
      m_os << "\"" << key << "\" :   ";
}

void JsonHandler1::addScalar(const string&  key, const string& value )
{
    int ival = 0;
    double dval=0.;

    addHead( key );

    if( value == "null" || value == "true" ||  value == "false" )
           m_os << value;
       else
        if( is<int>( ival, value.c_str()) || is<double>( dval, value.c_str()))
              m_os << value.c_str();
         else
            m_os << "\"" << value.c_str() << "\"";
}

} // namespace ParserYAML

} // namespace jsonio
