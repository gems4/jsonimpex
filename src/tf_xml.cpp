//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file v_xml.cpp
/// Implementation of functions for data exchange to/from XML format
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include <algorithm>
#include "pugixml.hpp"
#include "jsonimpex/tf_xml.h"
using namespace std;
using namespace pugi;

namespace jsonio {

namespace ParserXML
{
 void addArray( const string& key, const string& value, JsonDom* object );

 /// Print JsonNode structure to XML structure
 void object_emitter( pugi::xml_node& out, const JsonDom *object );

 void printNodeToXml(const char* file_name, const JsonDom *object )
 {
      pugi::xml_document doc;
      pugi::xml_node decl = doc.append_child(node_declaration);
      decl.append_attribute("version") = "1.0";
      decl.append_attribute("encoding") = "UTF-16";
      decl.append_attribute("standalone") = "yes";
      // define root node
      pugi::xml_node fld = doc.append_child("node");
      if( object->getType() == JSON_ARRAY )
          fld.append_attribute("isarray") = true;
      object_emitter( fld, object );
      doc.save_file(file_name);
  }

  void object_emitter( xml_node& out, const JsonDom *object )
  {
      xml_node ch1=out;
      int objtype = object->getType();
      size_t objsize = object->getChildrenCount();

      for( size_t ii=0; ii<objsize; ii++ )
      {
         auto childobj = object->getChild( ii);
         // before print
         switch( objtype )
         {
           case JSON_OBJECT:
             ch1 = out.append_child(childobj->getKey().c_str());
             break;
           case JSON_ARRAY:
             ch1 = out.append_child("element");
              break;
           default:
              break;
         }

         switch(childobj->getType())
          {
            // impotant datatypes
            case JSON_NULL:
                 ch1.append_child(node_null);
                break;
            case JSON_BOOL:
            case JSON_INT:
            case JSON_DOUBLE:
                 ch1.text().set( childobj->toString().c_str() );
                 break;
            case JSON_STRING: // ?? " bson_iterator_string(&i) "
                 ch1.text().set( childobj->toString().c_str() );
                 break;
            // main constructions
            case JSON_OBJECT:
                 object_emitter( ch1, childobj);
                 break;
            case JSON_ARRAY:
                 ch1.append_attribute("isarray") = true;
                 object_emitter(ch1, childobj );
                 break;
            default:
                ch1.text().set( "can't print type : " );
       }
    }
  }

  void parse( const pugi::xml_node& node, JsonDom* object  );


  // Read one XML object from text file and parse to bson structure
  bool parseXMLToNode( const char *fname, JsonDom* object  )
  {
      // read and parse xml document
      pugi::xml_document doc;
      pugi::xml_parse_result result = doc.load_file(fname);
      if (!result)
      {
          stringstream str;
          str << "XML [" << fname << "] parsed with errors " << endl;
          str << "Error description: " << result.description() << "\n";
          str << "Error offset: " << result.offset << " (error at [..." << (result.offset) << "]\n\n";
          jsonioErr( "XML parser", str.str() );
      }
      // parse xml to bson data
      auto node = doc.child("node");
      bool isarray = false;
      if( node.attribute("isarray").as_bool() == true )
          isarray = true;

      if( isarray )
          object->updateTypeTop( JSON_ARRAY );
      int ii=0;
      for (pugi::xml_node_iterator it = node.begin(); it != node.end(); ++it, ++ii )
      {
        if( it->type() != node_element )
           continue;
        if( isarray )
          it->set_name(to_string(ii).c_str());
        //cout << it->name() << endl;
        parse( *it, object  );
      }
      return true;
  }

  // add ',' separated array
  void addArray( const string& key, const string& value, JsonDom* object )
  {
      string val = "",str = value;
      size_t pos = str.find( ',' );
      int ii=0;

      JsonDom* childobj =  object->appendArray(  key );
      while( pos != string::npos )
      {
          val = str.substr(0, pos);
          str = str.substr( pos+1 );
          pos = str.find( ',' );
          strip_all(val, " \n\t\r\'\"");
          childobj->appendScalar( to_string(ii++), val );
      }
      strip_all(str, " \n\t\r\'\"");
      childobj->appendScalar( to_string(ii), str );
  }


  void parse( const pugi::xml_node& node, JsonDom* object  )
  {
      if( !node.first_child() )
      {
           object->appendNull( node.name() );
           return;
      }

      vector<string> keys;
      bool isobject = true;  // object list (all node_pcdata will be ignored )
      bool isarray = false;  // isobject ==  true and all names is same or attribut isarray == true
      for (pugi::xml_node_iterator it = node.begin(); it != node.end(); ++it)
      {
          if( it->type() == node_element )
            keys.push_back( it->name() );
      }

      if( keys.empty() )
          isobject = false;
      else
          if( node.attribute("isarray").as_bool() == true )
          {    isarray = true;
               isobject = false;
          }
          else  if( keys.size()>1 )
              {   auto pred = [&](const string& str ){ return str==keys[0]; };
                  if ( std::all_of(keys.begin()+1, keys.end(),  pred ) )
                  {    isarray = true;
                       isobject = false;
                  }
             }

      if( !isarray && !isobject ) // only internal text
      {
          string value = node.text().as_string();
          //if( !value.empty() )
          { if( node.attribute("isarray").as_bool() == true )
              addArray( node.name(), value, object );
            else
            {     strip_all(value, " \n\t\r\'\"");
                  object->appendScalar( node.name(), value );
            }
          }
      }
      else if( isobject )
         {
            JsonDom* childobj =  object->appendObject(  node.name() );
            for (pugi::xml_node_iterator it = node.begin(); it != node.end(); ++it)
            {
              if( it->type() != node_element )
                 continue;
              parse( *it, childobj  );
            }
         }
         else if( isarray )
             {
                JsonDom* childobj =  object->appendArray(  node.name() );
                int ii=0;
                for (pugi::xml_node_iterator it = node.begin(); it != node.end(); ++it, ++ii)
                {
                  if( it->type() != node_element )
                    continue;
                  it->set_name(to_string(ii).c_str());
                  parse( *it, childobj  );
                }
              }
  }

 } // ParserXML

} // namespace jsonio
