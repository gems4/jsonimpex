//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbclient.cpp
/// Implementation of class
/// TDBClient - DB driver for working with thrift server data base
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <cstring>
#include <iostream>
#include "jsonimpex/dbclient.h"
#include "jsonio/dbquerydef.h"
#include "jsonio/io_settings.h"
//using namespace std;

#include <thrift/transport/TSocket.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TProtocolException.h>

using namespace apache::thrift;
using namespace apache::thrift::transport;
using namespace apache::thrift::protocol;

#include "DBServer.h"
using namespace  dbserver;

namespace jsonio {

query::ConditionData toCondition(const DBQueryData& query );

/// Definition private of graph database chain used Thrift server
class ThriftClientAPI
{
    ::std::shared_ptr<apache::thrift::transport::TTransport> trans;
    dbserver::DBServerClient* dbClient = nullptr;

public:

    ///  Constructor
    ThriftClientAPI( const std::string& theHost, int thePort )
    {
      resetDBServerClient(theHost, thePort);
    }

    ///  Destructor
    ~ThriftClientAPI()
    {
       delete dbClient;
    }

    dbserver::DBServerClient* resetDBServerClient( const std::string& theHost, int thePort);

    /// Create collection if no exist
    void createCollection(const std::string& collname, const std::string& ctype);
    /// Collect collection names in current database ( or only Edges/Vertex collections )
    std::set<std::string> getCollectionNames_( jsonio::TAbstractDBDriver::CollectionTypes ctype );

   /// Create new record from a JSON representation of a single document
   std::string CreateRecord( const std::string& collname, const std::string& jsonrec );
   /// Read record by rid to JSON representation of a single document
   bool ReadRecord( const std::string& collname, const std::string& rid, std::string& jsonrec );
   /// Update existing document (a  JSON representation of a document update as an object)
   std::string UpdateRecord( const std::string& collname, const std::string& rid, const std::string& jsonrec );
   /// Removes the document identified by document-handle
   bool DeleteRecord( const std::string& collname, const std::string& rid );

   /// Run query
   ///  \param collname - collection name
   ///  \param query - query  condition (where)
   ///  \param setfnc - function for set up readed data
   void selectQuery( const std::string& collname, const DBQueryData& query,  SetReadedFunction setfnc );

   /// Execute function to multiple documents by their ids
   ///  \param collname - collection name
   ///  \param ids - list of _ids
   ///  \param setfnc - function for set up readed data
   void lookupByIds( const std::string& /*collname*/,  const std::vector<std::string>& /*ids*/,  SetReadedFunction /*setfnc*/ ){}


   /// Run query for read Linked records list
   ///  \param collname - collection name
   ///  \param queryFields - list of fileds selection
   ///  \param setfnc - function for set up Linked records list
    void allQuery( const std::string& collname,
           const std::set<std::string>& queryFields,  SetReadedFunctionKey setfnc );

    /// Run delete edges connected to vertex record
    ///  \param collname - collection name
    ///  \param vertexid - vertex record id
    void deleteEdges(const std::string& collname, const std::string& vertexid );

    /// Removes multiple documents by their ids
    ///  \param collname - collection name
    ///  \param ids - list of _ids
    void removeByIds( const std::string& /*collname*/,  const std::vector<std::string>& /*ids*/  ){}

    ///  Provides 'distinct' operation over collection
    ///  \param fpath Field path to collect distinct values from.
    ///  \param collname - collection name
    ///  \param return values by specified fpath and collname
    void fpathCollect( const std::string& collname, const std::string& fpath,
                             std::vector<std::string>& values );

};

query::ConditionData toCondition(const DBQueryData& query )
{
  query::ConditionData _condit;
  _condit.style = query::QueryStyle::QUndef;
  _condit.find = query.getQueryString();
  _condit.bind = query.getBindVars();
  _condit.fields = query.getQueryFields();

  switch( query.getType())
  {
    case DBQueryData::qTemplate:
        _condit.style = query::QueryStyle::QTemplate;
        break;
    case DBQueryData::qEdgesAll:
        _condit.style = query::QueryStyle::QEdgesAll;
        break;
    case DBQueryData::qEdgesFrom:
       _condit.style = query::QueryStyle::QEdgesFrom;
       break;
    case DBQueryData::qEdgesTo:
        _condit.style = query::QueryStyle::QEdgesTo;
        break;
   case DBQueryData::qUndef:
   case DBQueryData::qAQL:
       _condit.style = query::QueryStyle::QAQL;
       break;
    case DBQueryData::qEJDB:
       _condit.style = query::QueryStyle::QEJDB;
       break;
  }
 return _condit;
}

DBServerClient* ThriftClientAPI::resetDBServerClient(const std::string& theHost, int thePort)
{
   delete dbClient;

  //Set the Transport
   trans =  ::std::make_shared<TSocket>(theHost.c_str(), thePort );
   trans =  ::std::make_shared<TBufferedTransport>(trans);

   //Set the Protocol
   ::std::shared_ptr<TProtocol> proto;
   proto.reset(new TBinaryProtocol(trans));

   dbClient = new DBServerClient(proto);

   // Connect to service
   return dbClient ;
}

// Retrive one record from the collection
bool ThriftClientAPI::ReadRecord( const std::string& collname, const std::string& rid, std::string& jsonrec )
{
    // Get oid of record
    ValueResponse _data;
    std::string errejdb;

    // read from server
    try{
         trans->open();
         dbClient->ReadRecord(_data, collname, rid);
         trans->close();
    }
    catch (const TException & te)
    {
      std::cout << "TException: " << te.what() << std::endl;
      jsonioErr( "DBClient001", te.what() );
    }

    // test responseCode
    switch( _data.responseCode )
    {
       case ResponseCode::Success:
            jsonrec = _data.value;
            return true;
       case ResponseCode::NameNotFound:
            errejdb = "Collection doesn't exist";
            break;
       case ResponseCode::RecordNotFound:
            errejdb = "Record doesn't exist";
            break;
       case ResponseCode::Error:
             errejdb =  _data.errormsg;
                break;
       default: break;
    }
    jsonioErr( "DBClient001", errejdb );
    //return false;
}

// Removes record from the collection
bool ThriftClientAPI::DeleteRecord( const std::string& collname, const std::string& rid )
{
    ResponseCode::type _data;

    // read from server
    try{
         trans->open();
         _data = dbClient->DeleteRecord( collname, rid);
         trans->close();
    }
    catch (const TException & te)
    {
      std::cout << "TException: " << te.what() << std::endl;
      jsonioErr( "DBClient005", te.what() );
    }

    // test responseCode
    std::string errejdb;
    switch( _data )
    {
       case ResponseCode::Success:
            return true;
       case ResponseCode::NameNotFound:
            errejdb = "Collection doesn't exist";
            break;
       case ResponseCode::RecordNotFound:
            errejdb = "Record doesn't exist";
            break;
       case ResponseCode::Error:
            errejdb =  "Undefined error";
        break;
       default: break;
    }
    jsonioErr( "DBClient001", errejdb );
    //return false;
}

// Save/update record in the collection
std::string ThriftClientAPI::CreateRecord( const std::string& collname, const std::string& jsonrec )
{
    ResponseCode::type _ret;
    std::string errormsg = "Undefined error";
    std::string newid;

    // read from server
    try{
         trans->open();
         KeyResponse _return;
         dbClient->CreateRecord( _return, collname, jsonrec);
         _ret =_return.responseCode;
         if( _ret == ResponseCode::Success )
               newid = _return.key;
          else
             errormsg =  _return.errormsg;
         trans->close();
    }
    catch (const TException & te)
    {
      std::cout << "TException: " << te.what() << std::endl;
      jsonioErr( "DBClient006", te.what() );
    }

    // test responseCode
    switch( _ret )
    {
       case ResponseCode::Success:
            return newid;
       case ResponseCode::NameNotFound:
            errormsg = "Collection doesn't exist";
            break;
       case ResponseCode::Error:
       default: break;
    }
    jsonioErr( "DBClient006", errormsg );
    //return "";
}

// Save/update record in the collection
std::string ThriftClientAPI::UpdateRecord( const std::string& collname, const std::string& rid, const std::string& jsonrec )
{
    ResponseCode::type _ret;
    std::string errormsg = "Undefined error";
    std::string newid;

    // read from server
    try{
         trans->open();
         newid = rid;
        _ret = dbClient->UpdateRecord( collname, rid, jsonrec);
         trans->close();
    }
    catch (const TException & te)
    {
      std::cout << "TException: " << te.what() << std::endl;
      jsonioErr( "DBClient006", te.what() );
    }

    // test responseCode
    switch( _ret )
    {
       case ResponseCode::Success:
            return newid;
       case ResponseCode::NameNotFound:
            errormsg = "Collection doesn't exist";
            break;
       case ResponseCode::RecordNotFound:
            errormsg = "Record doesn't exist";
            break;
       case ResponseCode::Error:
       default: break;
    }
    jsonioErr( "DBClient006", errormsg );
    //return "";
}

// Create collection if no exist
void ThriftClientAPI::createCollection(const std::string& collname, const std::string& ctype)
{
    ResponseCode::type _data;
    CollectionType::type _ctype = CollectionType::CDocument;
    if( ctype == "vertex")
        _ctype = CollectionType::CVertex;
    else if( ctype == "edge")
        _ctype = CollectionType::CEdge;

    // read from server
    try{
         trans->open();
         _data = dbClient->addCollection( collname, _ctype);
         trans->close();
    }
    catch (const TException & te)
    {
      std::cout << "TException: " << te.what() << std::endl;
      jsonioErr( "DBClient005", te.what() );
    }

    // test responseCode
    if( _data!=ResponseCode::Success  )
       jsonioErr( "DBClient005", "Error when create collection" );

}

// Run query
//  \param collname - collection name
//  \param query -  query  condition (where)
//  \param setfnc - function for set up readed data
void ThriftClientAPI::selectQuery( const std::string& collname, const DBQueryData& query,  SetReadedFunction setfnc )
{
    /* ?? add collection
     try{
          trans->open();
          dbClient->addCollection( collname );
          trans->close();
     }
     catch (const TException & te)
     {
       cout << "TException: " << te.what() << endl;
       jsonioErr( "DBClient007", te.what() );
     }
     */

    // read from server
    DBRecordListResponse _data;
    try{
         trans->open();
         dbClient->SearchRecords(_data, collname, {}, toCondition(query) );
         trans->close();
    }
    catch (const TException & te)
    {
      std::cout << "TException: " << te.what() << std::endl;
      jsonioErr( "DBClient007", te.what() );
    }

    // test responseCode
    switch( _data.responseCode )
    {
       case ResponseCode::NameNotFound:
             jsonioErr( "DBClient002", "Collection doesn't exist" );
       case ResponseCode::Error:
             jsonioErr( "DBClient004", _data.errormsg );
       default: break;
    }

    //cout << _data.records.size() << " records in collection " << getKeywd() << endl;
    for (uint i = 0; i < _data.records.size(); ++i)
        setfnc( _data.records[i].value );
}

// Run query
//  \param collname - collection name
//  \param queryFields - list of fileds selection
//  \param setfnc - function for set up readed data
void ThriftClientAPI::allQuery( const std::string& collname, const std::set<std::string>& queryFields,
    SetReadedFunctionKey setfnc )
{
    /* ?? add collection
     try{
          trans->open();
          dbClient->addCollection( collname );
          trans->close();
     }
     catch (const TException & te)
     {
       cout << "TException: " << te.what() << endl;
       jsonioErr( "DBClient007", te.what() );
     }
     */

    // read from server
    DBRecordListResponse _data;
    try{
         trans->open();
         dbClient->SearchRecords(_data, collname, queryFields, toCondition(DBQueryData(DBQueryData::qAll)));
         trans->close();
    }
    catch (const TException & te)
    {
      std::cout << "TException: " << te.what() << std::endl;
      jsonioErr( "DBClient007", te.what() );
    }

    // test responseCode
    switch( _data.responseCode )
    {
       case ResponseCode::NameNotFound:
             jsonioErr( "DBClient002", "Collection doesn't exist" );
       case ResponseCode::Error:
             jsonioErr( "DBClient004", _data.errormsg );
       default: break;
    }

    //cout << _data.records.size() << " records in collection " << getKeywd() << endl;
    for (uint i = 0; i < _data.records.size(); ++i)
    {
         //cout << _data.records[i].c_str() << endl;
         setfnc(   _data.records[i].value, _data.records[i].key);
    }
}


void ThriftClientAPI::deleteEdges(const std::string& collname, const std::string& vertexid )
{
    ResponseCode::type _data;

    // read from server
    try{
         trans->open();
         _data = dbClient->DeleteEdges( collname, vertexid );
         trans->close();
    }
    catch (const TException & te)
    {
      std::cout << "TException: " << te.what() << std::endl;
      jsonioErr( "DBClient005", te.what() );
    }

    // test responseCode
    switch( _data )
    {
       case ResponseCode::NameNotFound:
             jsonioErr( "DBClient002", "Collection doesn't exist" );
       case ResponseCode::Error:
             jsonioErr( "DBClient004", "Undefined error" );
       default: break;
    }
}

void ThriftClientAPI::fpathCollect( const std::string& collname, const std::string& fpath,
                              std::vector<std::string>& values )
{

    /* ?? add collection
     try{
          trans->open();
          dbClient->addCollection( collname );
          trans->close();
     }
     catch (const TException & te)
     {
       cout << "TException: " << te.what() << endl;
       jsonioErr( "DBClient007", te.what() );
     }
     */

    // read from server
    StringListResponse _data;
    try{
         trans->open();
         dbClient->CollectValues(_data, collname, fpath );
         trans->close();
    }
    catch (const TException & te)
    {
      std::cout << "TException: " << te.what() << std::endl;
      jsonioErr( "DBClient007", te.what() );
    }

    // test responseCode
    switch( _data.responseCode )
    {
       case ResponseCode::NameNotFound:
             jsonioErr( "DBClient002", "Collection doesn't exist" );
       case ResponseCode::Error:
             jsonioErr( "DBClient004", _data.errormsg );
       default: break;
    }
    values = _data.names;
}


// Create collection if no exist
std::set<std::string> ThriftClientAPI::getCollectionNames_( TAbstractDBDriver::CollectionTypes ctype )
{
    std::set<std::string> collist;
    StringListResponse _return;

    CollectionType::type _ctype = CollectionType::CDocument;
    switch( ctype )
    {
      case TAbstractDBDriver::clVertex:
              _ctype = CollectionType::CVertex;
              break;
      case TAbstractDBDriver::clEdge:
             _ctype = CollectionType::CEdge;
             break;
      case TAbstractDBDriver::clAll:
             _ctype = CollectionType::CAll;
             break;
    }

    // read from server
    try{
         trans->open();
         dbClient->listCollections( _return, _ctype) ;
         trans->close();
    }
    catch (const TException & te)
    {
      std::cout << "TException: " << te.what() << std::endl;
      jsonioErr( "DBClient005", te.what() );
    }

    // test responseCode
    if( _return.responseCode != ResponseCode::Success )
      jsonioErr( "DBClient014", _return.errormsg );

    collist.insert(_return.names.begin() , _return.names.end() );
    return collist;
}


//    TDBClient -----------------------------------------------------

///  Constructor
TDBClient::TDBClient( const std::string& theHost, int thePort )
{
  resetDBServerClient(theHost, thePort);
}

//  Constructor from config file
TDBClient::TDBClient()
{
  std::string newHost =  ioSettings().value("jsonimpex.ThriftDBSocketHost",defHost );
  int newPort = ioSettings().value("jsonimpex.ThriftDBSocketPort", defPort );
  resetDBServerClient(newHost, newPort);
}

void TDBClient::resetDBServerClient(const std::string& theHost, int thePort)
{
  _host = theHost;
  _port = thePort;
  pdata.reset( new ThriftClientAPI(_host, _port) );
}

// Retrive one record from the collection
bool TDBClient::loadRecord( const std::string& collname, KeysSet::iterator& itr, std::string& jsonrec )
{
    std::string rid = getServerKey( itr->second.get() );
    return pdata->ReadRecord( collname, rid, jsonrec );
}

// Removes record from the collection
bool TDBClient::removeRecord( const std::string& collname,KeysSet::iterator& itr  )
{
    std::string rid = getServerKey( itr->second.get() );
    return pdata->DeleteRecord(collname, rid );
}

// Save/update record in the collection
std::string TDBClient::saveRecord( const std::string& collname, KeysSet::iterator& itr, const std::string& jsonrec )
{
  std::string newrid;
  if( itr->second.get() == nullptr ) // new record
    {
       newrid = pdata->CreateRecord( collname, jsonrec);
       if( !newrid.empty() )
         setServerKey( newrid, itr );
    }
    else
       {
          std::string rid = getServerKey( itr->second.get() );
          newrid = pdata->UpdateRecord( collname, rid, jsonrec );
        }
    return (newrid);
}

// Create collection if no exist
void TDBClient::createCollection(const std::string& collname, const std::string& ctype)
{
    pdata->createCollection(collname, ctype );
}

// list collection names
std::set<std::string> TDBClient::getCollectionNames( CollectionTypes ctype )
{
    return pdata->getCollectionNames_( ctype );
}


// Run query
//  \param collname - collection name
//  \param query -  query  condition (where)
//  \param setfnc - function for set up readed data
void TDBClient::selectQuery( const std::string& collname, const DBQueryData& query,  SetReadedFunction setfnc )
{
   pdata->selectQuery( collname,  query, setfnc );
}

void TDBClient::allQuery( const std::string& collname, const std::set<std::string>& queryFields,
    SetReadedFunctionKey setfnc )
{
    pdata->allQuery( collname, queryFields, setfnc );
}


void TDBClient::deleteEdges(const std::string& collname, const std::string& vertexid )
{
    pdata->deleteEdges( collname, vertexid );
}

void TDBClient::fpathCollect( const std::string& collname, const std::string& fpath,
                                    std::vector<std::string>& values )
{
   pdata->fpathCollect( collname, fpath, values);
}

void TDBClient::lookupByIds( const std::string& collname,  const std::vector<std::string>& ids,
                                   SetReadedFunction setfnc )
{
  // we can use ids and keys
   pdata->lookupByIds( collname, ids, setfnc);
}

void TDBClient::removeByIds( const std::string& collname,  const std::vector<std::string>& ids )
{
  // internal convertion from ids to keys
   pdata->removeByIds( collname, ids );
}


/* Gen new oid or other pointer of location
string TDBClient::genOid( const char* collname, const string& _keytemplate  )
{
    // Get oid of record
    string _oid;

    // read from server
    try{
         trans->open();
         dbClient->GenerateId(_oid, collname, _keytemplate );
         trans->close();
    }
    catch (const TException & te)
    {
      cout << "TException: " << te.what() << endl;
      jsonioErr( "DBClient001", te.what() );
    }
    return _oid;
}*/

} // namespace jsonio
