//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file thrift_import.h
/// Declarations of classes AbstractIEFile, StructDataIEFile, KeyValueIEFile -
/// implementation import of foreign format files
/// Prepared an impex.thrift file describing 4 file formats for import/export
/// of data to internal DOM based on our JSON schemas.
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef THRIFT_IMPORT_H
#define THRIFT_IMPORT_H

#include "jsonimpex/thrift_impex.h"
#include "jsonio/tf_json.h"
#include "impex_types.h"
#include "lua-run.h"

using namespace  ::impex;

namespace jsonio {

enum RUNMODE_ {
     modeImport = 0, modeExport = 1
   };


class JsonDomFree;
class FJsonYamlXmlArray;

class AbstractIEFile
{

protected:

    int _mode;
    const ThriftSchema* _schema;
    LuaRun _lua;

    /** Label of data type (vertex type), e.g. "datasource", "element" ... */
    std::string label;
    /** Export: the whole comment text; Import: the comment begin markup std::string (to skip until endl) */
    std::string comment;
    /** File name or "console" for export */
    std::string file_name;
    /** number of data block in file >=1, 0 if unknown */
    int Nblocks;
    /** number of text lines in file (>=1), 0 if unknown */
    int Nlines;

    /** Text block format in file corresponding to one database document (record) */
    FormatBlock block;

    // internal data

    /** DOM of label */
    JsonDomSchema *structNode;
    /** Key, Value pairs readed from the block of data in file*/
    ValuesMap externData;

    // internal functions

    /// Read data from one block in file
    virtual bool readBlockFromFile() =0;

    /// Save readed data to DOM
    virtual void setReadedData();

    /// Change number value, using script
    void run_lua_script( const std::string& script, std::vector<std::string>& values, bool isArray );
    /// Change std::string value, using script
    void run_lua_script_string( const std::string& script,  std::vector<std::string>& values, bool isArray );

    /// Change value, using convert
    void run_convert( const std::map<std::string, std::string>& convert, std::vector<std::string>& values );

    /// Run script for operation on data values in block
    void run_block_script( const std::string& script, JsonDomSchema* obj );

    /// Get Organization of object from the imported or exported file
    std::string impexFieldOrganization( const std::string& impexFieldKey );
    /// Get Type of object from the imported or exported file
    std::string impexFieldType( const std::string& impexFieldKey );

    /// Inser key value paies to extern data list
    void addMapKeyValue( const std::string& _key, const  std::vector<std::string>& vec );

    std::vector<std::string> extractValue( const std::string& key,
        const ValuesMap& writeData )
    {
       auto itval = writeData.find(key);
       if( itval != writeData.end() && itval->second.size() > 0 )
         return itval->second;
       else
         return {};
    }

    /// Asseble parts of group to one vector
    /// Extract data from different parts of thrift record
    void assembleGroups( ValuesMap& writeData  );


public:

    enum RUNMODE {
         modeImport = 0, modeExport = 1
       };

    AbstractIEFile( int amode, const ThriftSchema* aschema, const std::string& alualibPath):
      _mode(amode), _schema(aschema), _lua(alualibPath), structNode(nullptr)
    {}

    virtual ~AbstractIEFile()
    {
        closeFile();
        delete structNode;
    }

    /// Name of data type
    const std::string&  getDataName() const
    { return  label; }


    /// Open file to import
    virtual void openFile( const std::string& /*inputFname*/ ) {}
    /// Close internal file
    virtual void closeFile() {}

    /// Load string to import from ( or export to stringstream )
    virtual void loadString( const std::string& inputData );
    /// Get string export to  ( if export to stringstream )
    virtual std::string getString();

    /// Read one block to bson data
    bool readBlock( std::string& blockdata )
    {
     // read data from block to readedData
     externData.clear();
     if( !readBlockFromFile() )
       return false;

     // reset default structure
     structNode->clearField();
     // write data to structNode
     setReadedData();
     // Run script for operation on data values in block
     run_block_script(block.script, structNode );

     // return data to json std::string
     printNodeToJson( blockdata, structNode );
     return true;
    }

    virtual bool readBlock(  const std::string&, std::string&, std::string&, int  )
    {
      return false;
    }

    virtual bool nextFromFile( std::string& /*inputjson*/ )
    {
      return false;
    }


    // export part -----------------------------------------------------------------

    /// write data from one block to file
    virtual bool writeBlockToFile( const ValuesMap& /*writeData*/ ) =0 ;

    /// Extract data from bson
    virtual void extractDataToWrite( const std::string& blockdata, ValuesMap& writeData );

    /// Write one block to extern format file
    bool writeBlock( const std::string& blockdata )
    {
     externData.clear();
     extractDataToWrite( blockdata, externData );
     writeBlockToFile( externData );
     return true;
    }

};

//--------------------------------------------------------------------------
// Export/import of data records to/from text file in legacy JSON/YAML/XML (foreign keywords)
// Maybe using a generated JSON schema of that input file format?

/// Definition of foreign structured data JSON/YAML/XML text file
class  StructDataIEFile: public AbstractIEFile
{
   std::shared_ptr<FJsonYamlXmlArray> _stream;   ///< Stream to input/output

   /** Rendering syntax for the foreign file "JSON" | "YAML" | "XML" | ... */
   std::string renderer = "JSON";

  // internal values
   char _ftype;    ///< Input data type "JSON" | "YAML" | "XML" ...

  // internal functions

  /// Read data from one block in file
  bool readBlockFromFile();
  /// Read data one block from json data
  bool readBlockFromBson( const std::string& inputjson, std::string& key, int ndx=-1 );

 public:

   StructDataIEFile( int amode, const ThriftSchema* aschema, const std::string& alualibPath,
                     const FormatStructDataFile& defData );

   ~StructDataIEFile()
   {
     //bson_destroy(&xmlobj);
   }

   /// Open file to import
   void openFile( const std::string& inputFname );
   /// Close internal file
   void closeFile();

   /// Read next bson object
   bool nextFromFile( std::string& inputjson );

   /// Convert free format bson to schema defined
   bool readBlock(  const std::string& inputjson, std::string& schemajson, std::string& key, int ndx=-1 );

   // export part -----------------------------------------------------------------

   /// write data from one block to file
   virtual bool writeBlockToFile( const ValuesMap& /*writeData*/ )
   { return true; }

};


// http://eax.me/cpp-regex/
// https://regex101.com/

// bib
// "head_regexp" :   "^\\s*@article\\s*\\{\\s*(\\w+)\\s*,",
// "key_regexp" :   "^\\s*(\\w+)\\s*=\\s*",
// "value_regexp" :   "^\"([^\"]*)\"\\s*,{0,1}",
// "end_regexp" :   "^\\s*\\}",

// ris
// "head_regexp" :   "",
// "end_regexp" :   "^\\s*ER\\s*-\\s*",
// "key_regexp" :   "^\\s*([A-Z0-9]{2})\\s*-\\s*",
// "value_regexp" :   "",
// "value_next" :   "\r\n",

// Match a "quoted std::string"  : ^"(?:[^\\"]|\\.)*"$


//--------------------------------------------------------------------------
// Definition of text file with key-value pair data
class  KeyValueIEFile: public AbstractIEFile
{
    bool use_string_stream = false;
    std::fstream _fstream;      ///< Stream to input/output
    std::stringstream _stringstream;      ///< Stream to input/output
    std::istream& get_istream()
    {
        if( use_string_stream )
            return _stringstream;
        else
            return _fstream;
    }
    std::ostream& get_ostream()
    {
        if( use_string_stream )
            return _stringstream;
        else
            return _fstream;
    }

   /** Definition of key-value pair (line) in file */
   //FormatKeyValuePair lineFormat;
   /** Definition of key-value pair (line) in file */
   FormatKeyValue format;
   /** Rendering syntax for the foreign key-value file "GEMS3K" | "BIB" | "RIS" | ... */
   std::string renderer;
   /** Obsolute Definition of value, line, row, block, comment, end-of-data separators */
   Separators separs;

  // internal data
  std::string str_buf;  ///< Internal data to match regular expr
  std::string::const_iterator test_start;
  std::string::const_iterator test_end;

  // internal functions

  bool readBlockFromFile();
  bool readNextBlockfromFile();
  bool readRegexp( const std::string& re_str, std::vector<std::string>& mach_str );
  bool testRegexp( const std::string& re_str );
  bool readStartBlock();
  bool isEndBlock();
  std::string readAllValue();
  bool readKeyValuePair();
  void addKeyValue( const std::string& _key, const std::string& _value );
  void insertKeyValue( const std::string& _key, const std::vector<std::string>& vec );
  void skipComments();

 public:

   KeyValueIEFile( int amode, const ThriftSchema* aschema, const std::string& alualibPath,
                   const FormatKeyValueFile& defData );

   ~KeyValueIEFile()
   {}

   /// Open file to import
   void openFile( const std::string& inputFname );
   /// Close internal file
   void closeFile();

   /// Load string to import from ( or export to stringstream )
   void loadString( const std::string& inputData );
   /// Get string export to  ( if export to stringstream )
   std::string getString();

   /// Convert readed key-value data to schema defined bson
   bool initBlock(  std::string& blockdata, const ValuesMap& areadedData );
   const ValuesMap& getReadedData() const
   {
     return externData;
   }


   // export part -----------------------------------------------------------------

   /// write data from one block to file
   virtual bool writeBlockToFile( const ValuesMap& writeData );

};

//--------------------------------------------------------------------------
// Definition of text file with table data
class  TableIEFile: public AbstractIEFile
{
    bool use_string_stream = false;
    std::fstream _fstream;      ///< Stream to input/output
    std::stringstream _stringstream;      ///< Stream to input/output
    std::istream& get_istream()
    {
        if( use_string_stream )
            return _stringstream;
        else
            return _fstream;
    }
    std::ostream& get_ostream()
    {
        if( use_string_stream )
            return _stringstream;
        else
            return _fstream;
    }

   /** Definition of key-value pair (line) in file */
   FormatTable format;
   /** Rendering syntax for the foreign key-value file "GEMS3K" | "BIB" | "RIS" | ... */
   std::string renderer;
   /** Obsolute Definition of value, line, row, block, comment, end-of-data separators */
   Separators separs;

  // internal functions

  bool readBlockFromFile();
  bool readHeaderFromFile();
  void skipComments( std::string& row );
  std::string readNextRow();
  void addValue( size_t col, const std::string& _value, bool toheader);
  void splitFixedSize( const std::string& row, bool toheader=false);
  void splitRegExpr( const std::string& row, bool toheader=false);
  uint extractQuoted( const std::string& row, std::string& value );
  void splitDelimiter( const std::string& row, bool toheader=false);

 public:

   TableIEFile( int amode, const ThriftSchema* aschema, const std::string& alualibPath,
                   const FormatTableFile& defData );

   ~TableIEFile()
   {}

   /// Open file to import
   void openFile( const std::string& inputFname );
   /// Close internal file
   void closeFile();

   /// Load string to import from ( or export to stringstream )
   void loadString( const std::string& inputData );
   /// Get string export to  ( if export to stringstream )
   std::string getString();

   // export part -----------------------------------------------------------------

   /// write data from one block to file
   bool writeBlockToFile( const ValuesMap& writeData );

protected:

   // internal data
   std::string next_row_data;  ///< Internal data to match regular expr

   std::string read_next_data();

};


std::string parseCode(int etype);

} // namespace jsonio

#endif // THRIFT_IMPORT_H
