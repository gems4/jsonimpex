#include "jsonio/tf_json.h"
#include "jsonimpex/tf_yaml.h"
#include "jsonimpex/tf_xml.h"
#include "jsonimpex/yamlxml2file.h"

using namespace std;

namespace jsonio {

char FJsonYamlXml::defType = FileTypes::Json_;

// FJsonYamlXml --------------------------------------------------------

/// Load data from yaml file to bson object
void FJsonYamlXml::loadYaml( JsonDom* object )
{
    fstream fin(path_, ios::in );
    jsonioErrIf( !fin.good() , path_, "Fileopen error...");
    ParserYAML::parseYAMLToNode( fin, object );
}

/// Save data from bson object to yaml file
void FJsonYamlXml::saveYaml( const JsonDom* object ) const
{
    fstream fout(path_, ios::out);
    jsonioErrIf( !fout.good() , path_, "Fileopen error...");
    ParserYAML::printNodeToYAML( fout, object );
}

/// Load data from xml file to bson object
void FJsonYamlXml::loadXml( JsonDom* object )
{
    ParserXML::parseXMLToNode( path_.c_str(), object );
}

/// Save data from bson object to xml file
void FJsonYamlXml::saveXml( const JsonDom* object ) const
{
    ParserXML::printNodeToXml( path_.c_str(), object );
}


void FJsonYamlXml::LoadJson( JsonDom* object )
{
    switch( type_ )
    {
      case FileTypes::Json_:
          loadJson( object );
           break;
      case FileTypes::Yaml_:
           loadYaml( object );
           break;
      case FileTypes::XML_:
           loadXml( object );
           break;
    }
}

void FJsonYamlXml::SaveJson( const JsonDom* object ) const
{
    switch( type_ )
    {
      case FileTypes::Json_:
          saveJson(  object );
           break;
      case FileTypes::Yaml_:
           saveYaml(  object );
           break;
      case FileTypes::XML_:
           saveXml(  object );
           break;
    }
}

//---------------------------------------------------------------

// Open file to import
void FJsonYamlXmlArray::Open(int amode )
{
  mode_ = amode;
  arrObjects = shared_ptr<JsonDomFree>(JsonDomFree::newArray());
  loadedndx_ = 0;

  if( mode_ == OpenModeTypes::ReadOnly )
  {
    if( type_ == FileTypes::XML_ )
    {
      if( !ParserXML::parseXMLToNode( path_.c_str(), arrObjects.get() ))
             return;
    } else
      {
        // open  file for other types
        fstream _stream(path_,  std::fstream::in );
        jsonioErrIf( !_stream.good() , path_, "Fileopen error...");
        if( type_ == FileTypes::Yaml_ )
          {
             if( !ParserYAML::parseYAMLToNode( _stream, arrObjects.get() ))
             return;
          }
         if( type_ == FileTypes::Json_ )
          {
             if( !parseJsonToNode( _stream, arrObjects.get() ))
             return;
          }
      }
  }
  else
      if( mode_ != OpenModeTypes::WriteOnly )
        jsonioErr( path_, "Illegal file mode...");

  isopened_ = true;
}

// Close internal file
void FJsonYamlXmlArray::Close()
{
    if( mode_ == OpenModeTypes::WriteOnly )
    {
      if( type_ == FileTypes::XML_ )
          ParserXML::printNodeToXml( path_.c_str(), arrObjects.get() );
      else
      {
          fstream   _stream(path_,  std::fstream::out );
          jsonioErrIf( !_stream.good() , path_, "Output fileopen error...");
          if( type_ == FileTypes::Yaml_ )
              ParserYAML::printNodeToYAML( _stream, arrObjects.get()  );
          else
              if(  type_ == FileTypes::Json_ )
                   printNodeToJson( _stream, arrObjects.get()  ) ;
      }
    }
    isopened_ = false;
}

} // namespace jsonio



