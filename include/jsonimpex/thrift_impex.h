

#ifndef THRIFT_IMPEX_H
#define THRIFT_IMPEX_H

#include "jsonio/jsondomschema.h"

namespace jsonio {

///  Function that can be used to split text using regexp
std::vector<std::string> regexp_split(const std::string& str, std::string rgx_str = "\\s+");
///  Function that can be used to extract tokens using regexp
std::vector<std::string> regexp_extract(const std::string& str, std::string rgx_str);
///  Function that can be used to replase text using regexp
std::string regexp_replace(const std::string& instr, const std::string& rgx_str, const std::string& replacement );

class AbstractIEFile;

using ValuesMap = std::map<std::string,std::vector<std::string>>; ///< Map of values

/// Declarations of class ImpexFormatFile -
/// implementation import of foreign format files
/// Prepared an impex.thrift file describing 4 file formats for import/export
/// of data to internal DOM based on our JSON schemas.
class ImpexFormatFile
{
    /// ArangoDB connection data
     std::shared_ptr<AbstractIEFile> pdata;

public:

     enum RUNMODE {
          modeImport = 0, modeExport = 1
        };

     ImpexFormatFile( int amode, const std::string& impexSchemaName, const JsonDom* object );

     virtual ~ImpexFormatFile()
     { }

     /// Name of data type
     const std::string&  getDataName() const;


     /// Open file to import
     virtual void openFile( const std::string& inputFname );
     /// Close internal file
     virtual void closeFile();

     /// Load string to import from ( or export to stringstream )
     virtual void loadString( const std::string& inputData );
     /// Get string export to  ( if export to stringstream )
     virtual std::string getString();

     /// Read one block to json data
     bool readBlock( std::string& blockdata );
     /// Convert free format json to schema defined ( FormatStructDataFile only )
     bool readBlock(  const std::string& inputjson, std::string& schemajson, std::string& key, int ndx=-1 );
     /// Read next json object  ( FormatStructDataFile only )
     bool nextFromFile( std::string& inputjson );

     // export part -----------------------------------------------------------------

     /// write data from one block to file
     virtual bool writeBlockToFile( const ValuesMap& writeData );

     /// Extract data from Json
     virtual void extractDataToWrite( const std::string& blockdata, ValuesMap& writeData );

     /// Write one block to extern format file
     bool writeBlock( const std::string& blockdata );

};


} // namespace jsonio


#endif // THRIFT_IMPEX_H
