//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbclient.h
/// Declarations of class
/// TDBClient - DB driver for working with thrift server data base
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TDBCLIEBT_H
#define TDBCLIEBT_H

#include <cstring>
#include "jsonio/dbcollection.h"
#include "jsonio/dbquerydef.h"

namespace jsonio {

class ThriftClientAPI;

/// Default host that the socket is connected to
const std::string defHost = "localhost";

/// Default port that the socket is connected to
const int defPort = 9090;

/// Definition of graph database chain used Thrift server
class TDBClient : public TAbstractDBDriver
{
    /// The host that the socket is connected to
    std::string _host;
    /// The port that the socket is connected to
    int _port;

    /// ArangoDB connection data
    std::shared_ptr<ThriftClientAPI> pdata;

    void setServerKey( const std::string& key, KeysSet::iterator& itr )
    {
        char *bytes = new char[key.length()+1];
        strncpy( bytes, key.c_str(), key.length() );
        bytes[key.length()] = '\0';
        itr->second = std::unique_ptr<char>(bytes);
    }

public:

    ///  Constructor
    TDBClient( const std::string& theHost, int thePort );
    ///  Constructor from config file
    TDBClient();

    ///  Destructor
    virtual ~TDBClient()
    { }

    void resetDBServerClient( const std::string& theHost, int thePort);

    /// Create collection if no exist
    void createCollection(const std::string& collname, const std::string& ctype);
    /// Collect collection names in current database
    std::set<std::string> getCollectionNames( CollectionTypes ctype );

    std::string getServerKey( char* pars )
    {
      return std::string(pars);
    }

    std::string host() const
    {
      return _host;
    }
    int port() const
    {
      return _port;
    }

    // Gen new oid or other pointer of location
   // virtual string genOid(const char* collname, const std::string& _keytemplate );

   /// Read record from DB to json string
   virtual bool loadRecord( const std::string& collname, KeysSet::iterator& it, std::string& jsonrec );
   /// Remove current object from collection.
   virtual bool removeRecord(const std::string& collname, KeysSet::iterator& it );
   /// Write json record to DB
   virtual std::string saveRecord( const std::string& collname, KeysSet::iterator& it, const std::string& jsonrec );

   /// Run query
   ///  \param collname - collection name
   ///  \param query - json string with query  condition (where) and list of fileds selection and other
   ///  \param setfnc - function for set up readed data
   void selectQuery( const std::string& collname, const DBQueryData& query,  SetReadedFunction setfnc );

   /// Execute function to multiple documents by their ids
   ///  \param collname - collection name
   ///  \param ids - list of _ids
   ///  \param setfnc - function for set up readed data
   void lookupByIds( const std::string& collname,  const std::vector<std::string>& ids,  SetReadedFunction setfnc );


   /// Run query for read Linked records list
   ///  \param collname - collection name
   ///  \param queryFields - list of fileds selection
   ///  \param setfnc - function for set up Linked records list
    void allQuery( const std::string& collname,
           const std::set<std::string>& queryFields,  SetReadedFunctionKey setfnc );

    /// Run delete edges connected to vertex record
    ///  \param collname - collection name
    ///  \param vertexid - vertex record id
    virtual void deleteEdges(const std::string& collname, const std::string& vertexid );

    /// Removes multiple documents by their ids
    ///  \param collname - collection name
    ///  \param ids - list of _ids
    void removeByIds( const std::string& collname,  const std::vector<std::string>& ids  );

    ///  Provides 'distinct' operation over collection
    ///  \param fpath Field path to collect distinct values from.
    ///  \param collname - collection name
    ///  \param return values by specified fpath and collname
    virtual void fpathCollect( const std::string& collname, const std::string& fpath,
                             std::vector<std::string>& values );

};

} // namespace jsonio

#endif // TDBCLIEBT_H
