//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file/
/// Declarations&Implementation of TFileBase, FJson, ConfigJson,
/// ConfigArray - classes for file manipulation
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef YAMLXML2FILE_H
#define YAMLXML2FILE_H


#include "jsonio/json2file.h"

namespace jsonio {


/// Class for read/write json/yaml/xml files
class FJsonYamlXml : public FJson
{

protected:

   /// Load data from yaml file to bson object
   void loadYaml( JsonDom* object );

   /// Save data from bson object to yaml file
   void saveYaml( const JsonDom* object ) const;

   /// Load data from xml file to bson object
   void loadXml(  JsonDom* object );

   /// Save data from bson object to xml file
   void saveXml(  const JsonDom* object ) const;


public:

   static char defType;	///< Default cfg file type

    /// Constructor
    FJsonYamlXml( const std::string& fName, const std::string& fExt, const std::string& fDir=""):
       FJson( fName, fExt, fDir )
    {
        setType( getType(ext_) );
    }
    /// Constructor from path
    FJsonYamlXml( const std::string& path): FJson( path )
    {
        setType( getType(ext_) );
    }
    /// Constructor from bson data
    FJsonYamlXml( const JsonDom* object ):FJson( object ) {}
    /// Destructor
    ~FJsonYamlXml() { }

    /// Setup file type
    void setType(char ftype )
    {
        if( !( ftype == FileTypes::Json_ ||  ftype == FileTypes::Yaml_ || ftype == FileTypes::XML_ ) )
        {
            jsonioErr( "FJsonYamlXml", "Illegal file type" );
        }
        type_ = ftype;
    }

   /// Get file type from extension
   static char getType(const std::string& fExt )
   {
      char type__ =  defType;
      if(fExt == "json" )
        type__ = FileTypes::Json_;
      else  if(fExt == "yaml" )
              type__ = FileTypes::Yaml_;
      else if(fExt == "xml" )
              type__ = FileTypes::XML_;
      return type__;
   }


   /// Load data from file to dom object
   void LoadJson( JsonDom* object );

   /// Save data from bson object to file
   void SaveJson( const JsonDom* object ) const;

};

/// Class for read/write json/yaml/xml arrays files
class FJsonYamlXmlArray : public FJsonArray
{

public:

    /// Constructor
    FJsonYamlXmlArray( const std::string& fName, const std::string& fExt, const std::string& fDir=""):
       FJsonArray( fName, fExt, fDir )
    {
        setType( FJsonYamlXml::getType(ext_) );
    }
   /// Constructor from path
    FJsonYamlXmlArray( const std::string& path): FJsonArray( path )
    {
        setType( FJsonYamlXml::getType(ext_) );
    }
    /// Constructor from bson data
    FJsonYamlXmlArray( const JsonDom* object ):FJsonArray( object ) {}
    /// Destructor
    ~FJsonYamlXmlArray()
    { }

    /// Setup file type
    void setType(char ftype )
    {
        if( !( ftype == FileTypes::Json_ ||
               ftype == FileTypes::Yaml_ ||
               ftype == FileTypes::XML_ ) )
        {
            jsonioErr( "FJsonArray", "Illegal file type" );
        }
        type_ = ftype;
    }

   void Close();
   void Open(int amode );
};


} // namespace jsonio

#endif // YAMLXML2FILE_H
