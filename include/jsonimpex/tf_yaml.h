//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file v_yaml.h
/// Declarations of functions for data exchange to/from YAML format
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TF_YAML_H
#define TF_YAML_H

#include "jsonio/jsondom.h"

namespace jsonio {

namespace ParserYAML
{
    /// Print JsonNode structure/array to YAML string
    void printNodeToYAML( std::string& resStr, const JsonDom* object );

    /// Print JsonNode structure/array to YAML format file
    void printNodeToYAML( std::fstream& fout, const JsonDom* object );

    /// Parse one YAML object from string to JsonNode structure
    bool parseYAMLToNode( const std::string& currentYAML, JsonDom* object );

    /// Read one YAML object from text file and parse to JsonNode structure
    bool parseYAMLToNode( std::fstream& fin, JsonDom* object );

    /// Parse YAML string to json string
    std::string YAML2Json1( const std::string& currentYAML );
}

} // namespace jsonio

#endif // TF_YAML_H
