JSONIMPEX
=========

JSONIMPEX is a library and API providing a generic interface for exchanging the structured data between JSON, YAML, or XML files and ArangoDB multi-model database back-ends and for importing/exporting data from/to arbitrary foreign formats. This is extension jsonio library for using YAML and XML input/output file formats and for export/import  internal json schemas defined data using thrift defined converters.

* JSONIMPEX is written in C/C++ using open-source libraries jsonio, YAML-CPP, Pugixml, Thrift and Lua.
* Version: currently 0.1.
* Will be distributed as is (no liability) under the terms of Lesser GPL v.3 license. 

### How to get JSONIMPEX source code ###

* In your home directory, make a folder named e.g. ~/jsonimpex.
* cd ~/jsonimpex and clone this repository from https://bitbucket.org/gems4/jsonimpex.git  using a preinstalled free git client SourceTree or SmartGit (the best way on Windows). 
* Alternatively on Mac OS X or linux, open a terminal and type in the command line (do not forget a period):

```
git clone https://bitbucket.org/gems4/jsonimpex.git . 

```
## How to install the JSONIMPEX library ##

 Building JSONIMPEX requires g++, [CMake](http://www.cmake.org/).  

* Make sure you have g++, cmake and git installed. If not, install them. 

  On Ubuntu linux terminal, this can be done using the following commands:

```
#!bash
sudo apt-get install g++ cmake git libssl-dev libtool byacc flex
```

  For Mac OSX, make sure you have Homebrew installed (see [Homebrew web site](http://brew.sh) and [Homebrew on Mac OSX El Capitan](http://digitizor.com/install-homebrew-osx-el-capitan/) ).


* Install Dependencies

In order to build the JSONIMPEX library on (k)ubuntu linux 16.04/18.04 or MacOS, first execute the following: 

```
#!bash
cd ~/jsonimpex
sudo ./install-dependencies.sh
```

* Install the JSONIMPEX library

Then navigate to the directory where this README.md file is located and type in terminal:

```
cd ~/jsonimpex
sudo ./install.sh
```

* After that, headers, library  and the third-party libraries can be found in /usr/local/{include,lib}. 

### Install current version of ArangoDB server locally

On (K)Ubuntu linux, install the current version of ArangoDB server locally [from here](https://www.arangodb.com/download-major/ubuntu/):

~~~
curl -OL https://download.arangodb.com/arangodb34/DEBIAN/Release.key
sudo apt-key add - < Release.key
echo 'deb https://download.arangodb.com/arangodb34/DEBIAN/ /' | sudo tee /etc/apt/sources.list.d/arangodb.list
sudo apt-get install apt-transport-https
sudo apt-get update
sudo apt-get install arangodb3=3.4.4-1
~~~

The updates will come together with other ubuntu packages that you have installed.

On MacOS Sierra or higher, [navigate here](https://docs.arangodb.com/3.4/Manual/Installation/MacOSX.html) and follow the instructions on how to install ArangoDB with homebrew. Basically, you have to open a terminal and run two commands:

~~~
brew update
brew install arangodb
~~~


* TBD